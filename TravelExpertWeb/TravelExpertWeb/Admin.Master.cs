﻿/*
 Themplate: Customer
 Author: Dave Jookhuu, Fabian Isaza
 Date: July 15,2017
 Instractor: Jolanta.W
 Course: CPRG 214 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TravelExpertWeb
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //checks if user logged in
            if (Session["LoggedUser"] != null)
            {
                Response.Write(Session["CustomerID"]);        
                lblLoggedUser.Text = "Welcome "+Session["LoggedUser"].ToString() + "!";
                //Session["LoggedUser"] = null;
                //Response.Redirect("login.aspx");
            }       
        }
    }
}