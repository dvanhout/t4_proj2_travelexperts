﻿/*
 Themplate: Customer
 Author: Dave Jookhuu, Fabian Isaza
 Date: July 15,2017
 Instractor: Jolanta. W
 Course: CPRG 214 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelExpertWeb.App_Code
{
    public class Customer
    {
        //setting up Customer related properties
        public Customer() { }       

        public int CustomerId { get; set; }
        public string CustPass { get; set; }
        public string CustFirstName { get; set; }
        public string CustLastName { get; set; }
        public string CustAddress { get; set; }
        public string CustCity { get; set; }
        public string CustProv { get; set; }
        public string CustPostal { get; set; }
        public string CustCountry { get; set; }
        public string CustHomePhone { get; set; }
        public string CustBusPhone { get; set; }
        public string CustEmail { get; set; }
        public int AgentId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}