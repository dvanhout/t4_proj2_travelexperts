﻿ <%--
 Themplate: Customer
 Author: Dave Jookhuu, Fabian Isaza
 Date: July 15,2017
 Instractor: Jolanta. W
 Course: CPRG 214 
--%>
<%@ Page Title="" Language="C#" MasterPageFile="~/TravelExpert.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="TravelExpertWeb.WebForm4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    
    <div class="modal-dialog">
		<div class="loginmodal-container">
			<h1>Login to Your Account</h1><br>
			<form id="form1" runat="server">
                <asp:Label ID="lblUserName" runat="server" Text="User Name"></asp:Label>
                <asp:TextBox ID="txtUserName" placeholder="Username" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required Field" ControlToValidate="txtUserName" ForeColor="#990000"></asp:RequiredFieldValidator><br/>
                <asp:Label ID="lblInvalidUser" runat="server" Text=""></asp:Label><br/>
                <asp:Label ID="lblPass" runat="server" Text="Password"></asp:Label>
                <asp:TextBox ID="txtPass" placeholder="Password" runat="server" TextMode="Password"></asp:TextBox></br>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required Field" ControlToValidate="txtPass" ForeColor="#990000"></asp:RequiredFieldValidator>
                <asp:Label ID="lblInvalidPass" runat="server" Text=""></asp:Label><br/>
                <asp:Button CssClass="login loginmodal-submit" ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" />
			</form>					
			<div class="login-help">
			<a href="registration.aspx">Register</a>
			</div>
		</div>
	</div>   
</asp:Content>
