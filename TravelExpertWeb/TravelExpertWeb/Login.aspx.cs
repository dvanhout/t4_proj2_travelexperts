﻿/*
 Themplate: Customer
 Author: Dave Jookhuu, Fabian Isaza
 Date: July 15,2017
 Instractor: Jolanta. W
 Course: CPRG 214 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TravelExpertWeb.App_Code;

namespace TravelExpertWeb
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        string userName, password;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            userName = txtUserName.Text.Trim();
            password = txtPass.Text.Trim();
            //password = "mypass";

            //checking if user exists           
            if (CustomerDB.CheckUserIfExists(userName) == true)
            {
                //checks if logged user's CustomerID is set
                Customer custID = CustomerDB.CustomerLogin(userName, password);
                //Response.Write(custID.CustomerId.ToString());
                if (custID.CustomerId != 0)
                {
                    Customer tempCust = CustomerDB.CustomerLogin(userName, password);
                    Session["LoggedUser"] = userName;
                    Session["CustomerID"] = tempCust.CustomerId;
                    //Response.Write(custID.Password + " " + custID.CustomerId);
                    Response.Redirect("client-admin.aspx");
                    //Response.Redirect("customer-profile.aspx");
                }
                else
                {
                    lblInvalidPass.Text = "Incorrect password!";
                }

            }
            else
            {
                lblInvalidUser.Text = "Invalid username!";               

            }

        }   
    }
}