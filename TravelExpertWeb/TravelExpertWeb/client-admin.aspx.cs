﻿/*
 Themplate: Customer
 Author: Dave Jookhuu, Fabian Isaza
 Date: July 15,2017
 Instractor: Jolanta. W
 Course: CPRG 214 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TravelExpertWeb.App_Code;

namespace TravelExpertWeb
{
    public partial class WebForm7 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //method checks if LoggedUser session is, sets CustomerID session and prints greeting based on the logged user
            if (Session["LoggedUser"] != null)
            {
                Response.Write(Session["CustomerID"]);

                //Session["CustomerId"] = 143;
                //Session["LoggedUser"] = null;
                //Response.Redirect("login.aspx");           
                lblLoggedUser.Text = Session["LoggedUser"].ToString() + "!";
                lblNumberOfPurchases.Text = CustomerDB.GetNumberOfPurchasesByClient((int)Session["CustomerID"]).ToString();
                
            }
            else
            {
                //if user not logged in, sends to login page
                Response.Redirect("login.aspx");
            }
        }
    }
}