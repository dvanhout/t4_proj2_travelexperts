﻿ <%--
 Themplate: Customer
 Author: Dave Jookhuu, Fabian Isaza
 Date: July 15,2017
 Instractor: Jolanta. W
 Course: CPRG 214 
--%>
<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="client-info-update.aspx.cs" Inherits="TravelExpertWeb.WebForm9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Hello <small><asp:Label ID="lblLoggedUser" runat="server" Text=""></asp:Label></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            How can we help you today?
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-comments fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"></div>
                                        <div>Update Profile</div>
                                    </div>
                                </div>
                            </div>
                            <a href="client-info-update.aspx">
                                <div class="panel-footer">
                                    <span class="pull-left">Need to change your info?</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><%Response.Write(Session["TotalPurchase"]);%></div>
                                        <div>Total Purchases</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left"></span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-shopping-cart fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">
                                            <asp:Label ID="lblNumberOfPurchases" runat="server" Text=""></asp:Label></div>
                                        <div>Orders</div>
                                    </div>
                                </div>
                            </div>
                            <a href="client-purchase.aspx">
                                <div class="panel-footer">
                                    <span class="pull-left">View Purchase Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"></div>
                                        <div>Some Deals for You!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
    <%
    if (Session["LoggedUser"] != null)
    {
    %>
    <section>
            
           <div>
               <h2></h2>
               <form id="UpdateCustomerInfo" runat="server">
                   <div class="form-group">                        
                        <asp:Label ID="lblFNameUPdate" runat="server" Text="First Name"></asp:Label>
                        <asp:TextBox CssClass="form-control" ID="txtUpdateFirstName" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUpdateFirstName" ErrorMessage="Required Field" ForeColor="#990000"></asp:RequiredFieldValidator>
                   </div>
                   <div class="form-group"> 
                        <asp:Label ID="lblUpdateLastName" runat="server" Text="Last Name"></asp:Label>
                        <asp:TextBox CssClass="form-control" ID="txtUpdateLastName" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUpdateLastName" ErrorMessage="Required Field" ForeColor="#990000"></asp:RequiredFieldValidator>
                   </div>
                   <div class="form-group">
                        <asp:Label ID="lblCityUpdate" runat="server" Text="City"></asp:Label>
                        <asp:TextBox CssClass="form-control" ID="txtCityUpdate" runat="server"></asp:TextBox>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtCityUpdate" ErrorMessage="Required Field" ForeColor="#990000"></asp:RequiredFieldValidator>
                   </div>
                   <div class="form-group">
                       <asp:Label ID="lblAddressUpdate" runat="server" Text="Address"></asp:Label>
                       <asp:TextBox CssClass="form-control" ID="txtAddressUpdate" runat="server"></asp:TextBox>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Required Field" ForeColor="#990000" ControlToValidate="txtAddressUpdate"></asp:RequiredFieldValidator>
                   </div>
                   <div class="form-group  dropdown">
                       <asp:DropDownList ID="cboProvince" runat="server" AutoPostBack="True">
                         <asp:ListItem Selected="True" Value="AB">Alberta</asp:ListItem>
                         <asp:ListItem Value="ON">Ontario</asp:ListItem>
                         <asp:ListItem Value="BC">British Columbia</asp:ListItem>
                         <asp:ListItem Value="MB">Manitoba</asp:ListItem>
                         <asp:ListItem Value="SK">Saskatchewan</asp:ListItem>
                         <asp:ListItem Value="NS">Nova Scotia</asp:ListItem>
                         <asp:ListItem Value="QB">Quebec</asp:ListItem>
                         <asp:ListItem Value="NB">New Brunswick</asp:ListItem>
                         <asp:ListItem Value="NL">Newfoundland and Labrador</asp:ListItem>
                         <asp:ListItem Value="PE">Prince Edward Island</asp:ListItem>
                         <asp:ListItem Value="NT">Northwest Territories</asp:ListItem>
                         <asp:ListItem Value="NV">Nunavut</asp:ListItem>
                         <asp:ListItem Value=" YU"> Yukon</asp:ListItem>
                     </asp:DropDownList>
                    
                    <asp:RequiredFieldValidator ID="RequiredFieldProvince" runat="server" ControlToValidate="cboProvince" ErrorMessage="Required Field" ForeColor="#990000"></asp:RequiredFieldValidator>
                 </div>
                   <div class="form-group">
                        <asp:Label ID="lblPostaUpdate" runat="server" Text="Postal Code"></asp:Label>
                        <asp:TextBox CssClass="form-control" ID="txtPostaUpdate" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtPostaUpdate" ErrorMessage="Required Field" ForeColor="#990000"></asp:RequiredFieldValidator>
                        <br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtPostaUpdate" ErrorMessage="Format must be T3T 3T3 or T3T3T3" ForeColor="#990000" ValidationExpression="^[A-Za-z]\d[A-Za-z]\s?\d[A-Za-z]\d\s?$"></asp:RegularExpressionValidator>
                  </div>
                   <div class="form-group">
                       <asp:Label ID="lblCountryUpdate" runat="server" Text="Country"></asp:Label>
                       <asp:TextBox CssClass="form-control" ID="txtCountryUpdate" runat="server"></asp:TextBox>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtCountryUpdate" ErrorMessage="Required Field" ForeColor="#990000"></asp:RequiredFieldValidator>
                   </div>
                   <div class="form-group">
                        <asp:Label ID="lblEmailUpdate" runat="server" Text="Email"></asp:Label>
                        <asp:TextBox CssClass="form-control" ID="txtEmailUpdate" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtEmailUpdate" ErrorMessage="Invalid email format" ForeColor="#990000" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>&nbsp;<br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Required Field" ForeColor="#990000" ControlToValidate="txtEmailUpdate"></asp:RequiredFieldValidator>
                   </div>
                   <div class="form-group">
                        <asp:Label ID="lblPhoneUpdate" runat="server" Text="Phone"></asp:Label>
                        <asp:TextBox CssClass="form-control" ID="txtPhoneUpdate" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtPhoneUpdate" ErrorMessage="Must be xxx-xxx-xxxx format" ForeColor="#990000" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:RegularExpressionValidator>&nbsp;<br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Required Field" ForeColor="#990000" ControlToValidate="txtPhoneUpdate"></asp:RequiredFieldValidator>
                   </div>
                   <div class="form-group">
                       <asp:Label ID="lblBusPhoneUpdate" runat="server" Text="Business Number"></asp:Label>
                       <asp:TextBox CssClass="form-control" ID="txtBusPhoneUpdate" runat="server"></asp:TextBox>
                 </div>
           
                   <asp:Button CssClass="btn btn-primary" ID="btnUpdate" runat="server" Height="36px" OnClick="btnUpdate_Click" Text="Update" Width="141px" />
                 
        </form>
           </div>

       </section>   
    <%
    }
    %>
        </div>
    </div>
</asp:Content>
