﻿/*
 Themplate: Customer
 Author: Dave Jookhuu, Fabian Isaza
 Date: July 15,2017
 Instractor: Jolanta. W
 Course: CPRG 214 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TravelExpertWeb.App_Code;

namespace TravelExpertWeb
{
    public partial class WebForm9 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //on page load, method checks if LoggedUser session is, sets CustomerID session and prints greeting based on the logged user
            if (Session["LoggedUser"] != null)
            {
                Response.Write(Session["CustomerID"]);
                lblLoggedUser.Text = Session["LoggedUser"].ToString() + "!";
                lblNumberOfPurchases.Text = CustomerDB.GetNumberOfPurchasesByClient((int)Session["CustomerID"]).ToString();
                //Session["LoggedUser"] = null;
                //Response.Redirect("login.aspx");                
            }
            else
            {
                //if not logged in, sends back to login page
                Response.Redirect("login.aspx");
            }

            // on page load, if user is logged in and if CustomerID session is set, this method gets customer data and puts in the form for customer info update
            if (!IsPostBack)
            {
                Customer updateCust = new Customer();
                if (Session["CustomerID"] != null)
                {
                    Customer custInfo = CustomerDB.GetCustomerInfo((int)Session["CustomerID"]);
                    txtUpdateFirstName.Text = custInfo.CustFirstName;
                    txtUpdateLastName.Text = custInfo.CustLastName;
                    txtAddressUpdate.Text = custInfo.CustAddress;
                    txtCityUpdate.Text = custInfo.CustCity;
                    cboProvince.SelectedValue = custInfo.CustProv;
                    txtPostaUpdate.Text = custInfo.CustPostal;
                    txtCountryUpdate.Text = custInfo.CustCountry;
                    txtPhoneUpdate.Text = custInfo.CustHomePhone;
                    txtBusPhoneUpdate.Text = custInfo.CustBusPhone;
                    txtEmailUpdate.Text = custInfo.CustEmail;
                }

            }
        }

        //method checks if CustomerID session is set then this method updates customer information on update button is clicked 
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Customer updateCust = new Customer();

            //updates customer information
            if (Session["CustomerID"] != null)
            {
                updateCust.CustomerId = (int)Session["CustomerID"];
            }

            updateCust.CustFirstName = txtUpdateFirstName.Text.Trim();
            updateCust.CustLastName = txtUpdateLastName.Text.Trim();
            updateCust.CustAddress = txtAddressUpdate.Text.Trim();
            updateCust.CustCity = txtCityUpdate.Text.Trim();
            updateCust.CustProv = cboProvince.SelectedValue.Trim();
            updateCust.CustPostal = txtPostaUpdate.Text.Trim();
            updateCust.CustCountry = txtCountryUpdate.Text.Trim();
            updateCust.CustHomePhone = txtPhoneUpdate.Text.Trim();
            updateCust.CustBusPhone = txtBusPhoneUpdate.Text.Trim();
            updateCust.CustEmail = txtEmailUpdate.Text.Trim();
            CustomerDB.UpdateCustomer(updateCust);
        }        
    }
}