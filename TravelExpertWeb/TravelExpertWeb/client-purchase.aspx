﻿ <%--
 Themplate: Customer
 Author: Dave Jookhuu, Fabian Isaza
 Date: July 15,2017
 Instractor: Jolanta. W
 Course: CPRG 214 
--%>
<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="client-purchase.aspx.cs" Inherits="TravelExpertWeb.WebForm8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Hello <small><asp:Label ID="lblLoggedUser" runat="server" Text=""></asp:Label></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            How can we help you today?
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-comments fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"></div>
                                        <div>Update Profile</div>
                                    </div>
                                </div>
                            </div>
                            <a href="client-info-update.aspx">
                                <div class="panel-footer">
                                    <span class="pull-left">Need to change your info?</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">
                                            <asp:Label ID="lblTotalPurchases" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div>Total Purchases</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left"></span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-shopping-cart fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">
                                            <asp:Label ID="lblNumberOfPurchases" runat="server" Text=""></asp:Label></div>
                                        <div>Orders</div>
                                    </div>
                                </div>
                            </div>
                            <a href="client-purchase.aspx">
                                <div class="panel-footer">
                                    <span class="pull-left">View Purchase Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"></div>
                                        <div>Some Deals for You!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="table-responsive">
            <form id="form1" runat="server">
        <p>
            &nbsp;</p>
        <p>
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <br />
            <asp:GridView CssClass="table table-striped" ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1">
                <Columns>
                    <asp:BoundField DataField="CustomerId" HeaderText="Customer Id" SortExpression="CustomerId" />
                    <asp:BoundField DataField="PackageId" HeaderText="Package Id" SortExpression="PackageId" />
                    <asp:BoundField DataField="BookingId" HeaderText="Booking Id" SortExpression="BookingId" />
                    <asp:BoundField DataField="BookingDate" HeaderText="Booking Date" SortExpression="BookingDate" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="Destination" HeaderText="Destination" SortExpression="Destination" />
                    <asp:BoundField DataField="TravelerCount" HeaderText="Traveler Count" SortExpression="TravelerCount" />
                    <asp:BoundField DataField="BasePrice" HeaderText="Base Price" SortExpression="BasePrice" DataFormatString="{0:c}" />
                    <asp:BoundField DataField="Total_Package" HeaderText="Total Package" SortExpression="Total_Package" DataFormatString="{0:c}" />
                </Columns>
            </asp:GridView>
        </p>
        <p>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetPurchasedPackagesCustomer" TypeName="TravelExpertWeb.App_Code.CustomerDB">
                <SelectParameters>
                    <asp:SessionParameter DefaultValue="" Name="CustomerId" SessionField="CustomerID" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </p>
    </form>
        </div>
    </div>
</asp:Content>
