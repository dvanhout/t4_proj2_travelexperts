﻿/*
 Themplate: Customer
 Author: Dave Jookhuu, Fabian Isaza
 Date: July 15,2017
 Instractor: Jolanta. W
 Course: CPRG 214 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TravelExpertWeb.App_Code;

namespace TravelExpertWeb
{
    public partial class WebForm8 : System.Web.UI.Page
    {
        //displays purchased itmes based on CustomerID
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoggedUser"] != null)
            {
                Response.Write(Session["CustomerID"]);
                lblLoggedUser.Text = Session["LoggedUser"].ToString() + "!";
                lblNumberOfPurchases.Text = CustomerDB.GetNumberOfPurchasesByClient((int)Session["CustomerID"]).ToString();
                //Session["LoggedUser"] = null;
                //Response.Redirect("login.aspx");
                //FABIAN EDITED
                Session["TotalPurchase"] = TotalValue().ToString("c");
                lblTotalPurchases.Text = TotalValue().ToString("c");
            }
            else
            {
                Response.Redirect("login.aspx");
            }
        }

        //below method goes throuh total column in client-purchase.aspx and calculates the amount and returns total
        public decimal TotalValue()
        {
            decimal total = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                total += Convert.ToDecimal(GridView1.Rows[i].Cells[7].Text.Replace("$", ""));
            }
            return total;
        }
    }
}