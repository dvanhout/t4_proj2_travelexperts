﻿/*
 Themplate: Customer
 Author: Dave Jookhuu, Fabian Isaza
 Date: July 15,2017
 Instractor: Jolanta. W
 Course: CPRG 214 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TravelExpertWeb
{
    public partial class kill_session : System.Web.UI.Page
    {
        //once the user clicks log out this page gets loaded and clears all the session and sets to null
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["LoggedUser"] = null;
            Session["CustomerID"] = null;
            Session["TotalPurchase"] = null;
            Response.Redirect("default.aspx");
        }
    }
}