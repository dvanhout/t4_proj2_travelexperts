﻿ <%--
 Themplate: Customer
 Author: Dave Jookhuu, Fabian Isaza
 Date: July 15,2017
 Instractor: Jolanta. W
 Course: CPRG 214 
--%>
<%@ Page Title="" Language="C#" MasterPageFile="~/TravelExpert.Master" AutoEventWireup="true" CodeBehind="registration.aspx.cs" Inherits="TravelExpertWeb.WebForm1" %>
<asp:Content ID="RegistrationMainContent" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <!-- Services Section -->

    <section id="services">
        <div class="container">       
                
             <form id="RegisterCustomer" runat="server">
                 <h3>Thank you for registering with us!</h3> 
                 <br/><br/>
                 <div class="form-group">
                    <asp:Label ID="lblUseName" runat="server" Text="User Name"></asp:Label>
                     <asp:TextBox CssClass="form-control" ID="txtUserName" runat="server"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserName" ErrorMessage="Required Field" ForeColor="#990000"></asp:RequiredFieldValidator><br/>
                     <asp:Label ID="lblUserExists" runat="server" Text=""></asp:Label>
                 </div>
                 <div class="form-group">
                     <asp:Label ID="lblFName" runat="server" Text="First Name"></asp:Label>
                     <asp:TextBox CssClass="form-control" ID="txtFName" runat="server"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldFirstName" runat="server" ControlToValidate="txtFName" ErrorMessage="Required Field" ForeColor="#990000"></asp:RequiredFieldValidator>
                 </div>
                 <div class="form-group">
                     <asp:Label ID="lblLName" runat="server" Text="Last Name"></asp:Label>
                     <asp:TextBox CssClass="form-control" ID="txtLName" runat="server"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldLastName" runat="server" ControlToValidate="txtLName" ErrorMessage="Required Field" ForeColor="#990000"></asp:RequiredFieldValidator>
                 </div>                 
                 <div class="form-group">
                    <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                    <asp:TextBox CssClass="form-control" ID="txtCity" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldCity" runat="server" ControlToValidate="txtCity" ErrorMessage="Required Field" ForeColor="#990000"></asp:RequiredFieldValidator>
                 </div>
                 <div class="form-group">                 
                    <asp:Label ID="lbl" runat="server" Text="Address"></asp:Label>                    
                    <asp:TextBox CssClass="form-control" ID="txtAddress" runat="server"></asp:TextBox>                    
                    <asp:RequiredFieldValidator ID="RequiredFieldAddress" runat="server" ErrorMessage="Required Field" ForeColor="#990000" ControlToValidate="txtAddress"></asp:RequiredFieldValidator>
                </div>
                 <div class="form-group dropdown">                    
                     <asp:DropDownList ID="cboProvince" runat="server" AutoPostBack="True">
                         <asp:ListItem Selected="True" Value="AB">Alberta</asp:ListItem>
                         <asp:ListItem Value="ON">Ontario</asp:ListItem>
                         <asp:ListItem Value="BC">British Columbia</asp:ListItem>
                         <asp:ListItem Value="MB">Manitoba</asp:ListItem>
                         <asp:ListItem Value="SK">Saskatchewan</asp:ListItem>
                         <asp:ListItem Value="NS">Nova Scotia</asp:ListItem>
                         <asp:ListItem Value="QB">Quebec</asp:ListItem>
                         <asp:ListItem Value="NB">New Brunswick</asp:ListItem>
                         <asp:ListItem Value="NL">Newfoundland and Labrador</asp:ListItem>
                         <asp:ListItem Value="PE">Prince Edward Island</asp:ListItem>
                         <asp:ListItem Value="NT">Northwest Territories</asp:ListItem>
                         <asp:ListItem Value="NV">Nunavut</asp:ListItem>
                         <asp:ListItem Value=" YU"> Yukon</asp:ListItem>
                     </asp:DropDownList>
                    
                    <asp:RequiredFieldValidator ID="RequiredFieldProvince" runat="server" ControlToValidate="cboProvince" ErrorMessage="Required Field" ForeColor="#990000"></asp:RequiredFieldValidator>
                 </div>
                 <div class="form-group">
                    <asp:Label ID="lblPostal" runat="server" Text="Postal Code"></asp:Label>
                    <asp:TextBox CssClass="form-control" ID="txtPostal" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldPostal" runat="server" ControlToValidate="txtPostal" ErrorMessage="Required Field" ForeColor="#990000"></asp:RequiredFieldValidator>
                       
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtPostal" ErrorMessage="Format must be T3T 3T3 or T3T3T3" ForeColor="#990000" ValidationExpression="^[A-Za-z]\d[A-Za-z]\s?\d[A-Za-z]\d\s?$"></asp:RegularExpressionValidator>
                </div>
                 <div class="form-group">
                        <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label>
                        <asp:TextBox CssClass="form-control" ID="txtCountry" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldCountry" runat="server" ControlToValidate="txtCountry" ErrorMessage="Required Field" ForeColor="#990000"></asp:RequiredFieldValidator>
                 </div>
                 <div class="form-group">
                        <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                        <asp:TextBox CssClass="form-control" ID="txtEmail" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="Invalid email format" ForeColor="#990000" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>&nbsp;<br />
                        <asp:RequiredFieldValidator ID="RequiredFieldEmail" runat="server" ErrorMessage="Required Field" ForeColor="#990000" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                 </div>
                 <div class="form-group">
                        <asp:Label ID="lblPhone" runat="server" Text="Phone"></asp:Label>
                        <asp:TextBox CssClass="form-control" ID="txtPhone" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="PhoneValidator" runat="server" ControlToValidate="txtPhone" ErrorMessage="Must be xxx-xxx-xxxx format" ForeColor="#990000" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:RegularExpressionValidator>&nbsp;<br />
                        <asp:RequiredFieldValidator ID="RequiredFieldPhone" runat="server" ErrorMessage="Required Field" ForeColor="#990000" ControlToValidate="txtPhone"></asp:RequiredFieldValidator>
                </div>
                 <div class="form-group">
                        <asp:Label ID="lblFax" runat="server" Text="Business Number"></asp:Label>
                        <asp:TextBox CssClass="form-control" ID="txtBus" runat="server"></asp:TextBox>
                </div>
                 <div class="form-group">
                        <asp:Label ID="lblPass" runat="server" Text="Password"></asp:Label>
                        <asp:TextBox CssClass="form-control" ID="txtPass" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldPass" runat="server" ControlToValidate="txtPass" ErrorMessage="Required Field" ForeColor="#990000"></asp:RequiredFieldValidator>
                 </div>
                 <div class="form-group">
                        <asp:Label ID="lblPassRepeat" runat="server" Text="Repeat Password"></asp:Label>
                        <asp:TextBox CssClass="form-control" ID="txtPassRep" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:CompareValidator ID="PassCompareValidator" runat="server" ControlToCompare="txtPass" ControlToValidate="txtPassRep" ErrorMessage="Passwrod must mutch" ForeColor="#990000"></asp:CompareValidator>
                </div>
                 <div class="form-group">
                     <asp:Button ID="btnRegister" CssClass="btn btn-primary" runat="server" OnClick="Registration_Click" Text="Register" />
                 </div>
        </form>
           
    </div>
</section>
</asp:Content>