﻿/*
 Themplate: Customer
 Author: Dave Jookhuu, Fabian Isaza
 Date: July 15,2017
 Instractor: Jolanta. W
 Course: CPRG 214 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using TravelExpertWeb.App_Code;

namespace TravelExpertWeb
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        //Guid CustID = Guid.NewGuid();
        //int agentID = 100;
        string firstName, lastName, city, address, province, postalCode, country, email, homePhone, busPhone, userName, password;

        protected void Page_Load(object sender, EventArgs e)
        {
           

        }        

        protected void Registration_Click(object sender, EventArgs e)
        {

            //setting up user inputs
            firstName = txtFName.Text.Trim();
            lastName = txtLName.Text.Trim();
            address = txtAddress.Text.Trim();
            city = txtCity.Text.Trim();
            province = cboProvince.SelectedValue.Trim();
            postalCode = txtPostal.Text.Trim();
            country = txtCountry.Text.Trim();
            homePhone = txtPhone.Text.Trim();
            busPhone = txtBus.Text.Trim();
            email = txtEmail.Text.Trim();
            userName = txtUserName.Text.Trim();
            password = txtPass.Text.Trim();
            //Response.Write(province);
            
            //checking if UserName exists
            if (CustomerDB.CheckUserIfExists(userName) == true)
            {
                lblUserExists.Text = "User alreay exists. Please choose a different one.";
            }
            else
            {
                CustomerDB.RegisterCustomer(firstName, lastName, address, city, province, postalCode, country, homePhone, busPhone, email, 7, userName, password);
                Response.Redirect("login.aspx");
            }
        }
                
        
    }
}