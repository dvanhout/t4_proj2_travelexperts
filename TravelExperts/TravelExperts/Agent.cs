﻿/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  Defines an Agent
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts {
    public class Agent {
        public int AgentId { get; set; }
        public string AgtFirstName { get; set; }
        public string AgtMiddleInitial { get; set; }
        public string AgtLastName { get; set; }
        public string AgtBusPhone { get; set; }
        public string AgtEmail { get; set; }
        public string AgtPosition { get; set; }
        public int? AgencyId { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }

        public Agent() { }
        public Agent(int agentId, string fName, string mInitial, string lName, 
                     string bPhone, string email, string pos, int agencyId, 
                     string pwd, string user) {
            AgentId = agentId;
            AgtFirstName = fName;
            AgtMiddleInitial = mInitial;
            AgtLastName = lName;
            AgtBusPhone = bPhone;
            AgtEmail = email;
            AgtPosition = pos;
            AgencyId = agencyId;
            Password = pwd;
            Username = user;
        } 
    }
}
