﻿/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  Database methods for Agents table in TravelExperts db 
 *               NOTE:  for the purposes of this assignment, no encryption
 *               was added to the password fields.  This would and must
 *               be done if this were to be used in a real application
 */

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts {
    public static class AgentDB {
        // return true if Agent username and password match record in the table
        public static bool LoginOk(string username , string password) { 
            SqlConnection con = TravelExpertsDB.GetConnection();
            string selectStatement = "SELECT Count(*) " +
                                     "FROM Agents " +
                                     "WHERE Username = @Username " +
                                     "AND Password = @Password";
            SqlCommand selectCommand = new SqlCommand(selectStatement, con);
            selectCommand.Parameters.AddWithValue("@Username", username);
            selectCommand.Parameters.AddWithValue("@Password", password);

            try {
                con.Open();
                int count = Convert.ToInt32(selectCommand.ExecuteScalar().ToString());
                if (count > 0) return true;
                else return false;
            } catch (SqlException ex) { throw ex; } 
            finally { con.Close(); }
        }


        // returns agent from by username/password
        public static Agent GetAgent(string username, string password) {
            Agent agt;
            SqlConnection con = TravelExpertsDB.GetConnection();
            string selectStatement = "SELECT AgentId, AgtFirstName, AgtMiddleInitial, " +
                                     "    AgtLastName, AgtBusPhone, " +
                                     "    AgtEmail, AgtPosition, " +
                                     "    AgencyId, Password, Username " +
                                     "FROM Agents WHERE " +
                                     "Password = @Password AND Username = @Username";
            SqlCommand selectCommand = new SqlCommand(selectStatement, con);
            selectCommand.Parameters.AddWithValue("@Password", password);
            selectCommand.Parameters.AddWithValue("@Username", username);

            try {
                con.Open();
                SqlDataReader r = selectCommand.ExecuteReader();
                if (r.Read()) {
                    agt = new Agent();
                    agt.AgentId = (int)r["AgentId"];
                    if (r["AgtFirstName"] != DBNull.Value) { agt.AgtFirstName = (string)r["AgtFirstName"]; } 
                        else agt.AgtFirstName = null;
                    if (r["AgtMiddleInitial"] != DBNull.Value) { agt.AgtMiddleInitial = (string)r["AgtMiddleInitial"]; } 
                        else agt.AgtMiddleInitial = null;
                    if (r["AgtLastName"] != DBNull.Value) { agt.AgtLastName = (string)r["AgtLastName"]; } 
                        else agt.AgtLastName = null;
                    if (r["AgtBusPhone"] != DBNull.Value) { agt.AgtBusPhone = (string)r["AgtBusPhone"]; } 
                        else agt.AgtBusPhone = null;
                    if (r["AgtEmail"] != DBNull.Value) { agt.AgtEmail = (string)r["AgtEmail"]; } 
                        else agt.AgtEmail = null;
                    if (r["AgtPosition"] != DBNull.Value) { agt.AgtPosition = (string)r["AgtPosition"]; } 
                        else agt.AgtPosition = null;
                    if (r["AgtFirstName"] != DBNull.Value) { agt.AgencyId = (int)r["AgencyId"]; } 
                        else agt.AgencyId = null;
                    agt.Password = (string)r["Password"];
                    agt.Username = (string)r["Username"];   
                    return agt;
                } else { return null; }
            } catch (SqlException ex) { throw ex; }
            finally { con.Close(); }
        }


        // updates an agent based on agentId, returns success/fail
        public static bool UpdateAgent(int agentId, Agent newAgent) {
            SqlConnection con = TravelExpertsDB.GetConnection();
            string updateStatement = "UPDATE Agents " +
                                     "SET AgtFirstName = @AgtFirstName, AgtMiddleInitial = @AgtMiddleInitial, " +
                                     "    AgtLastName = @AgtLastName, AgtBusPhone = @AgtBusPhone, " +
                                     "    AgtEmail = @AgtEmail, AgtPosition = @AgtPosition, " +
                                     "    AgencyId = @AgencyId, Password = @Password, Username = @Username " +
                                     "WHERE AgentId = @AgentId";
            SqlCommand updateCommand = new SqlCommand(updateStatement, con);
            updateCommand.Parameters.AddWithValue("@AgtFirstName", newAgent.AgtFirstName);
            updateCommand.Parameters.AddWithValue("@AgtMiddleInitial", newAgent.AgtMiddleInitial ?? "NULL");
            updateCommand.Parameters.AddWithValue("@AgtLastName", newAgent.AgtLastName);
            updateCommand.Parameters.AddWithValue("@AgtBusPhone", newAgent.AgtBusPhone);
            updateCommand.Parameters.AddWithValue("@AgtEmail", newAgent.AgtEmail);
            updateCommand.Parameters.AddWithValue("@AgtPosition", newAgent.AgtPosition);
            updateCommand.Parameters.AddWithValue("@AgencyId", newAgent.AgencyId);
            updateCommand.Parameters.AddWithValue("@Password", newAgent.Password);
            updateCommand.Parameters.AddWithValue("@Username", newAgent.Username);
            updateCommand.Parameters.AddWithValue("@AgentId", newAgent.AgentId);

            try {
                con.Open();
                int count = updateCommand.ExecuteNonQuery();
                if (count > 0) return true;
                else return false;
            } catch (SqlException ex ) { throw ex; }
            finally { con.Close(); }
        }
    }
}
