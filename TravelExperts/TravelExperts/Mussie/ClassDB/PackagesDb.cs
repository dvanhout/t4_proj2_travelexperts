﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TravelExperts.Mussie.Classes;

/*
 * Author: Mussie Tekste
 * Date: July, 2017
 * Description:  
 */

namespace TravelExperts.Mussie.ClassDB
{
    public static class PackagesDb
    {

        //method to get Packages
        public static TravelExperts.Mussie.Classes.Packages getPackage(int packageID)
        {
            SqlConnection con = TravelExperts.TravelExpertsDB.GetConnection();
            string query = "SELECT * FROM  packages where " +
                            "packageID=@packageID";
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.Add("@packageID", SqlDbType.Int).Value = packageID;
            con.Open();
            SqlDataReader reader = com.ExecuteReader();
            Packages package = new Packages();
            while (reader.Read())
            {

                package.PackageID = Convert.ToInt16(reader[0].ToString());
                package.PkgName = reader[1].ToString();
                bool flagOD = (reader["pkgStartDate"] != System.DBNull.Value);
                if (flagOD)
                {
                    package.PkgStartDate = (DateTime)(reader["pkgStartDate"]);
                }
                else
                {
                    package.PkgStartDate = DateTime.MinValue;
                }
                flagOD = (reader["pkgEndDate"] != System.DBNull.Value);
                if (flagOD)
                {
                    package.PkgEndDate = (DateTime)(reader["pkgEndDate"]);
                }
                else
                {
                    package.PkgEndDate = DateTime.MinValue;
                }
                flagOD = (reader["pkgDesc"] != System.DBNull.Value);
                if (flagOD)
                {
                    package.PkgDesc = (reader["pkgDesc"]).ToString();
                }
                else
                {
                    package.PkgDesc = "";
                }
                decimal money = Math.Round(Convert.ToDecimal(reader["pkgBasePrice"].ToString()));
                package.PkgBasePrice = money;
                flagOD = (reader["PkgAgencyCommission"] != System.DBNull.Value);
                if (flagOD)
                {
                    decimal commision = Math.Round(Convert.ToDecimal(reader["PkgAgencyCommission"].ToString()));
                    package.PkgAgencyCommission = commision;
                }
                else
                {
                    package.PkgAgencyCommission = 0;
                }
            }
            con.Close();
            return package;
        }


         //get Package IDs from Database
       public static DataTable getPackageIDs()
        {
            SqlConnection con = TravelExperts.TravelExpertsDB.GetConnection();
            string query = "select PackageId,PkgName from Packages";
            SqlCommand com = new SqlCommand(query, con);
            con.Open();
            SqlDataReader reader = com.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }



        //used for updating data in Database
        public static int updatePackage(Packages package)
        {
            SqlConnection con = TravelExperts.TravelExpertsDB.GetConnection();
            string query = "UPDATE packages SET " +
                 " pkgStartDate = @PkgStartDate, " +
                 " pkgEndDate = @PkgEndDate," +
                 " pkgDesc = @PkgDesc," +
                 " pkgBasePrice =  @PkgBasePrice," +
                 " pkgAgencyCommission =  @PkgAgencyCommission " +
                 "where " +
                 " packageID = @PackageID";
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("@pkgEndDate", package.PkgEndDate);
            com.Parameters.AddWithValue("@pkgStartDate", package.PkgStartDate);
            com.Parameters.AddWithValue("@pkgDesc", package.PkgDesc);
            com.Parameters.AddWithValue("@pkgBasePrice", package.PkgBasePrice);
            com.Parameters.AddWithValue("@pkgAgencyCommission", package.PkgAgencyCommission);
            com.Parameters.AddWithValue("@packageID", package.PackageID);

            con.Open();
            int success = com.ExecuteNonQuery();

            con.Close();
            return success;
        }


        public static bool AddNewProductSupplierToPackage(int product_supplierID)
        {
            //using the funcntion from the parameter call
            SqlConnection connection = TravelExperts.TravelExpertsDB.GetConnection();
            string selectStatement = "SELECT IDENT_CURRENT('Packages') FROM Packages";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            int newPackageId = 0;
            try
            {
                connection.Open();
                newPackageId = Convert.ToInt32(selectCommand.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.GetType() + ex.Message);
            }
            finally
            {
                connection.Close();
            }
            string insertStatement = @"INSERT INTO Packages_Products_Suppliers VALUES (@PackageId, @ProductSupplierId)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, connection);
            insertCommand.Parameters.AddWithValue("@PackageId", newPackageId);
            insertCommand.Parameters.AddWithValue("@ProductSupplierId", product_supplierID);
            try
            {
                connection.Open();
                insertCommand.ExecuteNonQuery();
                return true;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.GetType() + ex.Message);
                return false;
            }
            finally
            {
                connection.Close();
            }
        }


        public static List<TravelExperts.Mussie.Classes.Product> GetProducts()
        {
            SqlConnection connection = TravelExperts.TravelExpertsDB.GetConnection();
            string selectStatement = "SELECT ProductId, ProdName " +
                                     "FROM Products ORDER BY ProdName Asc";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            connection.Open();
            SqlDataReader reader = selectCommand.ExecuteReader();

            var products = new List<TravelExperts.Mussie.Classes.Product>();
            while (reader.Read())
            {
                //for each record fill out a product
                var product = new TravelExperts.Mussie.Classes.Product();
                product.ProductId = Convert.ToInt32(reader["ProductId"]);
                product.ProdName = reader["ProdName"].ToString();
                products.Add(product);
            }

            connection.Close();
            return products;
        }


        //show existing items in Package as a listview
        public static List<PackageProductDetails> GetPackageProductDetails(int packageID)
        {
            SqlConnection connection = TravelExperts.TravelExpertsDB.GetConnection();
            string selectStatement = "select * from Products_Suppliers ps inner join Products p on " +
             " ps.ProductId = p.ProductId inner join Suppliers s on " +
             " ps.SupplierId = s.SupplierId where ps.ProductSupplierId in ( " +
             "select productSupplierId from Packages_Products_Suppliers where @PackageId = PackageID)";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@PackageId", packageID);
            connection.Open();
            SqlDataReader reader = selectCommand.ExecuteReader();

            var products = new List<PackageProductDetails>();
            while (reader.Read())
            {
                //for each record fill out a product details for  that package
                PackageProductDetails product = new PackageProductDetails();
                product.supplierName = reader["SupName"].ToString();
                product.prodName = reader["ProdName"].ToString();
                product.ProductId = Convert.ToInt16(reader["ProductId"]);
                products.Add(product);
            }

            connection.Close();
            return products;
        }


        public static List<TravelExperts.Mussie.Classes.Supplier> GetAllSupplier(int ProductID)
        {

            SqlConnection con = TravelExperts.TravelExpertsDB.GetConnection();
            string query = "select * from Products_Suppliers ps inner join Suppliers s " +
                                "on ps.supplierid = s.supplierid  where @ProductId = ProductID";
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("@ProductId", ProductID);
            con.Open();
            SqlDataReader reader = com.ExecuteReader();
            List<TravelExperts.Mussie.Classes.Supplier> suplist = new List<TravelExperts.Mussie.Classes.Supplier>();

            while (reader.Read())
            {
                TravelExperts.Mussie.Classes.Supplier sup = new TravelExperts.Mussie.Classes.Supplier();
                sup.SupplierId = Convert.ToInt16(reader["SupplierId"]);
                sup.SupName = reader["SupName"].ToString();
                sup.ProductSupplierId = Convert.ToInt16(reader["ProductSupplierId"]);
                suplist.Add(sup);
            }
            con.Close();
            return suplist;
        }

       public static int addProductSupplierToPackage(int productSupplierId, int packageId)
        {

            SqlConnection connection = TravelExperts.TravelExpertsDB.GetConnection();
            string updatePkgDetails = @"INSERT INTO Packages_Products_Suppliers VALUES (@PackageId, @ProductSupplierId)";
            SqlCommand ins = new SqlCommand(updatePkgDetails, connection);
            ins.Parameters.AddWithValue("@PackageId", packageId);
            ins.Parameters.AddWithValue("@ProductSupplierId", productSupplierId);
            try
            {
                connection.Open();
                int success = ins.ExecuteNonQuery();
                return success;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.GetType() + ex.Message);
                return -1;//update failed
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
