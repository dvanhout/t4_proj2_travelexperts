﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


/*
 * Author: Mussie Tekste
 * Date: July, 2017
 * Description:  
 */


namespace TravelExperts.Mussie.ClassDB
{
 
    //Class for updating and editing
    public static class ProductDB
    {
 
        //Pull all products
        public static List<TravelExperts.Mussie.Classes.Product> GetProducts()
        {
            SqlConnection connection = TravelExperts.TravelExpertsDB.GetConnection();
            string selectStatement = "SELECT ProductId, ProdName " +
                                     "FROM Products ORDER BY ProdName Asc";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            connection.Open();
            SqlDataReader reader = selectCommand.ExecuteReader();

            var products = new List<TravelExperts.Mussie.Classes.Product>();
            while (reader.Read())
            {
                var product = new TravelExperts.Mussie.Classes.Product();
                product.ProductId = Convert.ToInt32(reader["ProductId"]);
                product.ProdName = reader["ProdName"].ToString();
                products.Add(product);
            }
            connection.Close();
            return products;
        }


        //Show product's current supplier
        public static List<TravelExperts.Mussie.Classes.ProductSupplier> GetProductSuppliers(TravelExperts.Mussie.Classes.Product product)
        {
            SqlConnection connection = TravelExperts.TravelExpertsDB.GetConnection();
            string selectStatement = "SELECT ps.SupplierId, SupName, ProductSupplierId FROM Products_Suppliers ps " +
                                     "INNER JOIN Suppliers s " +
                                     "ON s.SupplierId = ps.SupplierId " +
                                     "WHERE ProductId = @ProductId " +
                                     "ORDER BY SupName ASC";

            SqlCommand command = new SqlCommand(selectStatement, connection);
            command.Parameters.AddWithValue("@ProductId", product.ProductId);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            var prodSuppliers = new List<TravelExperts.Mussie.Classes.ProductSupplier>();
            while (reader.Read())
            {
                var prodSupplier = new TravelExperts.Mussie.Classes.ProductSupplier();

                prodSupplier.ProductSupplierId = Convert.ToInt32(reader["ProductSupplierId"]);
                prodSupplier.SupName = reader["SupName"].ToString();
                prodSupplier.SupplierId = Convert.ToInt32(reader["SupplierId"]);
                prodSuppliers.Add(prodSupplier);

            }
            connection.Close();
            return prodSuppliers;
        }
    }
}
    
