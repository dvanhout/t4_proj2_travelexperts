﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Author: Mussie Tekeste
 * Date: July, 2017
 * Description:  
 */
namespace TravelExperts.Mussie.Classes
{
    public class PackageProductDetails
    {
    
        public string prodName { get; set; }
        public string supplierName { get; set; }
        public int ProductId { get; set; }
    }
}
