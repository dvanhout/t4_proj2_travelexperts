﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelExperts.Mussie.ClassDB;

/*
 * Author: Mussie Tekeste
 * Date: July, 2017
 * Description:  
 */
namespace TravelExperts.Mussie.Classes
{
    public class Product
    {
        public int ProductId { get; set; } 
        public string ProdName { get; set; }
        public override string ToString()
        {
            return ProdName;
        }

    }
}
