﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelExperts.Mussie.ClassDB;

/*
 * Author: Mussie Tekeste
 * Date: July, 2017
 * Description:  
 */

namespace TravelExperts.Mussie.Classes
{
    public class ProductSupplier
    {
        public int ProductSupplierId { get; set; }
        public int SupplierId { get; set; }
        public string SupName { get; set; }
        public override string ToString()
        {
            return SupName;
        }
    
    }
}