﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Author: Mussie Tekeste
 * Date: July, 2017
 * Description:  
 */

namespace TravelExperts.Mussie.Classes
{
    public class Supplier
    {
        public int SupplierId { get; set; }
        public string SupName { get; set; }
        public int ProductSupplierId { get; set; }
    }
}
