﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TravelExperts.Mussie.Classes;
using TravelExperts.Mussie.ClassDB;
/*
 * Author: Mussie Tekeste
 * Date: July, 2017
 * Description: Form to Add New Package 
 */

namespace TravelExperts.Mussie.Forms
{
    public partial class frmAddPackage : Form
    {
        public frmAddPackage()
        {
            InitializeComponent();
        }

        private void frmAddPackage_Load(object sender, EventArgs e)
        {
            //load the product when the form  loads
            cboProduct.DataSource = ProductDB.GetProducts();
            //cboProduct.Items.Insert(0, new ListViewItem("--Please Select Package--", "0"));
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //get all the datat on the selected product and supplier to add to package
            TravelExperts.Mussie.Classes.Product selectedProduct = (TravelExperts.Mussie.Classes.Product)cboProduct.SelectedItem;
            ProductSupplier selectedSupplier = (ProductSupplier)lbSuppliers.SelectedItem;
            // creating a new listview Item 
            string newProduct = selectedProduct.ProdName;
            string newSupplier = selectedSupplier.SupName;
            int newProductSupplierId = selectedSupplier.ProductSupplierId;
            string[] row = { newProduct, newSupplier, newProductSupplierId.ToString() };
            var listViewItem = new ListViewItem(row);
            lvPackageProductSuppliers.Items.Add(listViewItem);

            List<ProductSupplier> remainingSuppliers = RemainingSuppliersforProduct();
            //check if the count of the List<> is 0, and disbale button add
            //will refresh the datasource regardless
            if (remainingSuppliers.Count != 0)
            {
                lbSuppliers.DataSource = remainingSuppliers;
                btnAdd.Enabled = true;
            }
            else
            {
                lbSuppliers.DataSource = remainingSuppliers;
                btnAdd.Enabled = false;
            }
        }

        //close add package form
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //remove product from package
        private void btnRemove_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvPackageProductSuppliers.SelectedItems)
            {
                lvPackageProductSuppliers.Items.Remove(item);
            }

            List<ProductSupplier> remainingSuppliers = RemainingSuppliersforProduct();
            lbSuppliers.DataSource = remainingSuppliers;

            if (remainingSuppliers.Count != 0)
            {
                btnAdd.Enabled = true;
            }
            else
            {
                btnAdd.Enabled = false;
            }
        }

        //save package in database
        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (ValidData())//validate data input
            {
                //putting the package information into the database 
                Package newPackage = new Package();
                newPackage.PkgName = Convert.ToString(txtPackageName.Text);
                newPackage.PkgStartDate = dtpStartDate.Value;
                newPackage.PkgEndDate = Convert.ToDateTime(dtpEndDate.Text);
                newPackage.PkgDesc = Convert.ToString(txtDescription.Text);
                newPackage.PkgBasePrice = Convert.ToDecimal(txtBasePrice.Text);
                newPackage.PkgAgencyCommission = Convert.ToDecimal(txtCommission.Text);

                //success or fail of package addition
                if (PackageDB.AddNewPackage(newPackage))
                {
                    MessageBox.Show("Package has been created in the database!", "Success");
                }
                else
                {
                    MessageBox.Show("An Error has occured in the database", "Error");
                }

                foreach (ListViewItem item in lvPackageProductSuppliers.Items)
                {
                    int newProductSupplierId = Convert.ToInt32(item.SubItems[2].Text);

                    if (PackageDB.AddNewProductSupplierToPackage(newProductSupplierId))
                    {

                    }
                    else
                    {
                        MessageBox.Show("Error with this supplier", "Database Error");
                    }
                }
                DialogResult = DialogResult.OK;
            }
        }



        //get a list of all the suppliers associated with the product 
        private List<ProductSupplier> RemainingSuppliersforProduct()
        {           
            TravelExperts.Mussie.Classes.Product selectedProduct = (TravelExperts.Mussie.Classes.Product)cboProduct.SelectedItem;
            List<ProductSupplier> SuppliersForProduct = ProductDB.GetProductSuppliers(selectedProduct);
            List<int> productSupplierIds = new List<int>();

            foreach (ListViewItem item in lvPackageProductSuppliers.Items)
            {
                int productSupplierId = Convert.ToInt32(item.SubItems[2].Text);
                productSupplierIds.Add(productSupplierId); // adding this to the list of suppliers for the product
            }
            var suppliersNotIn = from supplier in SuppliersForProduct
                                 where !(productSupplierIds.Any(packagesupplier => packagesupplier == supplier.ProductSupplierId))
                                 select supplier;

            List<ProductSupplier> suppliersRemaining = new List<ProductSupplier>();//second list for keeping track of remaining supplier  
            foreach (var supplier in suppliersNotIn)
            {
                suppliersRemaining.Add(supplier);
            }

            return suppliersRemaining;
        }

        //method for validators
        private bool ValidData()
        {
            return
            Validator.IsPresent(txtPackageName) &&
            Validator.ValidDate(dtpStartDate.Value, dtpEndDate.Value) &&
            Validator.IsPresent(txtDescription) &&
            Validator.IsPresent(txtBasePrice) &&
            Validator.IsDecimal(txtBasePrice) &&
            Validator.IsWithinRange(txtBasePrice, 0, 10000000) &&
            Validator.IsPresent(txtCommission) &&
            Validator.IsDecimal(txtCommission) &&
            Validator.IsWithinRange(txtCommission, 0, 10000000) &&
            Validator.CommissionBasePriceCheck(Convert.ToDecimal(txtBasePrice.Text), Convert.ToDecimal(txtCommission.Text));
        }

        private void cboProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<ProductSupplier> remainingSuppliers = RemainingSuppliersforProduct();
            if (remainingSuppliers.Count != 0)
            {
                lbSuppliers.DataSource = remainingSuppliers;
                btnAdd.Enabled = true;
            }
            else
            {
                lbSuppliers.DataSource = remainingSuppliers;
                btnAdd.Enabled = false;//disable add button when no products to choose from
            }
        }

        //clear package add form fields
        private void btnClear_Click(object sender, EventArgs e)
        {
            txtPackageName.Text = "";
            txtDescription.Text = "";
            dtpStartDate.Text = "";
            dtpEndDate.Text = "";
            txtBasePrice.Text = "";
            txtCommission.Text = "";
        }
    }
}
