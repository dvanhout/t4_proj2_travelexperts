﻿namespace TravelExperts.Mussie.Forms
{
    partial class frmUpdatePackage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUpdatePackage));
            this.btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lblComission = new System.Windows.Forms.Label();
            this.lblBasePrice = new System.Windows.Forms.Label();
            this.txtCommission = new System.Windows.Forms.TextBox();
            this.txtBasePrice = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.lblPackageName = new System.Windows.Forms.Label();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.lvSelectProductSupplier = new System.Windows.Forms.ListView();
            this.Product_Update = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Supplier_Update = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ProductSupplierId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.Supplier = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Product = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lbSuppliers = new System.Windows.Forms.ListBox();
            this.cbProducts = new System.Windows.Forms.ComboBox();
            this.lvPackageDetails = new System.Windows.Forms.ListView();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.cbPackageName = new System.Windows.Forms.ComboBox();
            this.rtbPkgDesc = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(157, 425);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(169, 34);
            this.btnCancel.TabIndex = 74;
            this.btnCancel.Text = "Exit";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(354, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 19);
            this.label1.TabIndex = 73;
            this.label1.Text = "Package Currently Contains: ";
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnUpdate.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnUpdate.Location = new System.Drawing.Point(154, 367);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(170, 42);
            this.btnUpdate.TabIndex = 72;
            this.btnUpdate.Text = "Update Package";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // lblComission
            // 
            this.lblComission.AutoSize = true;
            this.lblComission.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComission.Location = new System.Drawing.Point(18, 303);
            this.lblComission.Name = "lblComission";
            this.lblComission.Size = new System.Drawing.Size(132, 19);
            this.lblComission.TabIndex = 70;
            this.lblComission.Text = "Agency Commision";
            // 
            // lblBasePrice
            // 
            this.lblBasePrice.AutoSize = true;
            this.lblBasePrice.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBasePrice.Location = new System.Drawing.Point(18, 267);
            this.lblBasePrice.Name = "lblBasePrice";
            this.lblBasePrice.Size = new System.Drawing.Size(77, 19);
            this.lblBasePrice.TabIndex = 71;
            this.lblBasePrice.Text = "Base Price";
            // 
            // txtCommission
            // 
            this.txtCommission.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCommission.Location = new System.Drawing.Point(158, 300);
            this.txtCommission.Name = "txtCommission";
            this.txtCommission.Size = new System.Drawing.Size(169, 27);
            this.txtCommission.TabIndex = 69;
            // 
            // txtBasePrice
            // 
            this.txtBasePrice.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBasePrice.Location = new System.Drawing.Point(158, 267);
            this.txtBasePrice.Name = "txtBasePrice";
            this.txtBasePrice.Size = new System.Drawing.Size(169, 27);
            this.txtBasePrice.TabIndex = 68;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(18, 135);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(83, 19);
            this.lblDescription.TabIndex = 64;
            this.lblDescription.Text = "Description";
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndDate.Location = new System.Drawing.Point(18, 99);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(68, 19);
            this.lblEndDate.TabIndex = 65;
            this.lblEndDate.Text = "End Date";
            // 
            // lblPackageName
            // 
            this.lblPackageName.AutoSize = true;
            this.lblPackageName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPackageName.Location = new System.Drawing.Point(18, 41);
            this.lblPackageName.Name = "lblPackageName";
            this.lblPackageName.Size = new System.Drawing.Size(105, 19);
            this.lblPackageName.TabIndex = 66;
            this.lblPackageName.Text = "Package Name";
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartDate.Location = new System.Drawing.Point(18, 66);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(74, 19);
            this.lblStartDate.TabIndex = 67;
            this.lblStartDate.Text = "Start Date";
            // 
            // lvSelectProductSupplier
            // 
            this.lvSelectProductSupplier.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Product_Update,
            this.Supplier_Update,
            this.ProductSupplierId});
            this.lvSelectProductSupplier.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvSelectProductSupplier.Location = new System.Drawing.Point(689, 196);
            this.lvSelectProductSupplier.Name = "lvSelectProductSupplier";
            this.lvSelectProductSupplier.Size = new System.Drawing.Size(253, 213);
            this.lvSelectProductSupplier.TabIndex = 63;
            this.lvSelectProductSupplier.UseCompatibleStateImageBehavior = false;
            this.lvSelectProductSupplier.View = System.Windows.Forms.View.Details;
            // 
            // Product_Update
            // 
            this.Product_Update.Text = "Product";
            this.Product_Update.Width = 118;
            // 
            // Supplier_Update
            // 
            this.Supplier_Update.Text = "Supplier";
            this.Supplier_Update.Width = 127;
            // 
            // ProductSupplierId
            // 
            this.ProductSupplierId.Text = "ProductSupplierId";
            this.ProductSupplierId.Width = 3;
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEndDate.Location = new System.Drawing.Point(158, 99);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(166, 27);
            this.dtpEndDate.TabIndex = 57;
            this.dtpEndDate.Tag = "End Date";
            // 
            // Supplier
            // 
            this.Supplier.Text = "Supplier";
            this.Supplier.Width = 165;
            // 
            // Product
            // 
            this.Product.Text = "Product";
            this.Product.Width = 164;
            // 
            // btnRemove
            // 
            this.btnRemove.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.Location = new System.Drawing.Point(597, 300);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(86, 44);
            this.btnRemove.TabIndex = 62;
            this.btnRemove.Text = "Remove<<";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(597, 250);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(86, 44);
            this.btnAdd.TabIndex = 61;
            this.btnAdd.Text = "Add>>";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lbSuppliers
            // 
            this.lbSuppliers.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSuppliers.FormattingEnabled = true;
            this.lbSuppliers.ItemHeight = 19;
            this.lbSuppliers.Location = new System.Drawing.Point(348, 196);
            this.lbSuppliers.Name = "lbSuppliers";
            this.lbSuppliers.Size = new System.Drawing.Size(243, 213);
            this.lbSuppliers.TabIndex = 60;
            // 
            // cbProducts
            // 
            this.cbProducts.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbProducts.FormattingEnabled = true;
            this.cbProducts.Location = new System.Drawing.Point(693, 31);
            this.cbProducts.Name = "cbProducts";
            this.cbProducts.Size = new System.Drawing.Size(238, 27);
            this.cbProducts.TabIndex = 59;
            this.cbProducts.SelectedIndexChanged += new System.EventHandler(this.cbProducts_SelectedIndexChanged);
            // 
            // lvPackageDetails
            // 
            this.lvPackageDetails.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Product,
            this.Supplier});
            this.lvPackageDetails.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvPackageDetails.Location = new System.Drawing.Point(348, 32);
            this.lvPackageDetails.Name = "lvPackageDetails";
            this.lvPackageDetails.Size = new System.Drawing.Size(335, 143);
            this.lvPackageDetails.TabIndex = 58;
            this.lvPackageDetails.UseCompatibleStateImageBehavior = false;
            this.lvPackageDetails.View = System.Windows.Forms.View.Details;
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpStartDate.Location = new System.Drawing.Point(158, 66);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(166, 27);
            this.dtpStartDate.TabIndex = 56;
            this.dtpStartDate.Tag = "Start Date";
            // 
            // cbPackageName
            // 
            this.cbPackageName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPackageName.FormattingEnabled = true;
            this.cbPackageName.Location = new System.Drawing.Point(158, 33);
            this.cbPackageName.Name = "cbPackageName";
            this.cbPackageName.Size = new System.Drawing.Size(166, 27);
            this.cbPackageName.TabIndex = 55;
            this.cbPackageName.SelectedIndexChanged += new System.EventHandler(this.cbPackageName_SelectedIndexChanged);
            // 
            // rtbPkgDesc
            // 
            this.rtbPkgDesc.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbPkgDesc.Location = new System.Drawing.Point(158, 132);
            this.rtbPkgDesc.MaxLength = 50;
            this.rtbPkgDesc.Name = "rtbPkgDesc";
            this.rtbPkgDesc.Size = new System.Drawing.Size(169, 76);
            this.rtbPkgDesc.TabIndex = 76;
            this.rtbPkgDesc.Tag = "Description";
            this.rtbPkgDesc.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(689, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(179, 19);
            this.label2.TabIndex = 77;
            this.label2.Text = "Available Products to Add:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(154, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 19);
            this.label4.TabIndex = 78;
            this.label4.Text = "Select Package to Update";
            // 
            // frmUpdatePackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(949, 471);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rtbPkgDesc);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.lblComission);
            this.Controls.Add(this.lblBasePrice);
            this.Controls.Add(this.txtCommission);
            this.Controls.Add(this.txtBasePrice);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblEndDate);
            this.Controls.Add(this.lblPackageName);
            this.Controls.Add(this.lblStartDate);
            this.Controls.Add(this.lvSelectProductSupplier);
            this.Controls.Add(this.dtpEndDate);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lbSuppliers);
            this.Controls.Add(this.cbProducts);
            this.Controls.Add(this.lvPackageDetails);
            this.Controls.Add(this.dtpStartDate);
            this.Controls.Add(this.cbPackageName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmUpdatePackage";
            this.Text = "Update Package";
            this.Load += new System.EventHandler(this.frmUpdatePackage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label lblComission;
        private System.Windows.Forms.Label lblBasePrice;
        private System.Windows.Forms.TextBox txtCommission;
        private System.Windows.Forms.TextBox txtBasePrice;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.Label lblPackageName;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.ListView lvSelectProductSupplier;
        private System.Windows.Forms.ColumnHeader Product_Update;
        private System.Windows.Forms.ColumnHeader Supplier_Update;
        private System.Windows.Forms.ColumnHeader ProductSupplierId;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.ColumnHeader Supplier;
        private System.Windows.Forms.ColumnHeader Product;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListBox lbSuppliers;
        private System.Windows.Forms.ComboBox cbProducts;
        private System.Windows.Forms.ListView lvPackageDetails;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.ComboBox cbPackageName;
        private System.Windows.Forms.RichTextBox rtbPkgDesc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
    }
}