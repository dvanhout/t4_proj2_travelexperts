﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using TravelExperts.Mussie.Classes;
using TravelExperts.Mussie.ClassDB;

/*
 * Author: Mussie Tekeste
 * Date: July, 2017
 * Description:  
 */

namespace TravelExperts.Mussie.Forms
{
    public partial class frmUpdatePackage : Form
    {
        public frmUpdatePackage()
        {
            InitializeComponent();
        }

        private void frmUpdatePackage_Load(object sender, EventArgs e)
        {
            //get products for combobox
            cbProducts.DataSource = PackagesDb.GetProducts();
            //cbProducts.SelectedValue = "-Please Select Package-";
            cbProducts.DisplayMember = "ProdName";
            cbProducts.ValueMember = "ProductId";

            //get packages for combobox
            cbPackageName.DataSource = PackagesDb.getPackageIDs();
            cbPackageName.DisplayMember = "PkgName";
            cbPackageName.ValueMember = "PackageId";

            //put specific data on the fileds 
            Packages package = null;
            package = PackagesDb.getPackage(Convert.ToInt16(cbPackageName.SelectedValue));
            dtpStartDate.Value = package.PkgStartDate;
            dtpEndDate.Value = package.PkgEndDate;
            rtbPkgDesc.Text = package.PkgDesc;
            String CostConv = (string.Format(("{0:N}"), package.PkgBasePrice)); // remove currency sign from base price
            txtBasePrice.Text = CostConv;
            CostConv = (string.Format(("{0:N}"), package.PkgAgencyCommission)); // remove currency sign from commision price
            txtCommission.Text = CostConv;

            //get dates from database
            if (package.PkgStartDate == DateTime.MinValue)
            {
                dtpStartDate.CustomFormat = " ";
                dtpStartDate.Format = DateTimePickerFormat.Custom;
            }
            else
            {
                dtpStartDate.Format = DateTimePickerFormat.Long;
                dtpStartDate.Value = package.PkgStartDate;
            }
            if (package.PkgEndDate == DateTime.MinValue)
            {
                dtpEndDate.CustomFormat = " ";
                dtpEndDate.Format = DateTimePickerFormat.Custom;
            }
            else
            {
                dtpEndDate.Format = DateTimePickerFormat.Long;
                dtpEndDate.Value = package.PkgEndDate;
            }

            //populate listview for package details
            List<PackageProductDetails> ppd = new List<PackageProductDetails>();
            ppd = PackagesDb.GetPackageProductDetails(Convert.ToInt16(cbPackageName.SelectedValue));
            foreach (var item in ppd)
            {
                lvPackageDetails.Items.Add(new ListViewItem(new string[] { item.prodName, item.supplierName }));
            }

            //populate listbox for suppliers for a certain product
            List<TravelExperts.Mussie.Classes.Supplier> sup = new List<TravelExperts.Mussie.Classes.Supplier>();
            sup = PackagesDb.GetAllSupplier(Convert.ToInt16(cbProducts.SelectedValue));
            foreach (var item in sup)
            {
                lbSuppliers.Items.Add(item);
                lbSuppliers.DisplayMember = "SupName";
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (lbSuppliers.SelectedItem == null)
            {
                MessageBox.Show("Please select a supplier.");
            }

            else
            {
                //adding values into the listview that you want to update to the package
                TravelExperts.Mussie.Classes.Supplier s1 = (TravelExperts.Mussie.Classes.Supplier)lbSuppliers.SelectedItem;
                string supplier = s1.SupName;
                TravelExperts.Mussie.Classes.Product p = (TravelExperts.Mussie.Classes.Product)cbProducts.SelectedItem;
                string product = p.ProdName;
                string productSupplierId = s1.ProductSupplierId.ToString();
                ListViewItem lvi = new ListViewItem(new string[] { product, supplier, productSupplierId });
                lvSelectProductSupplier.Items.Add(lvi);
                lbSuppliers.Items.Remove(lbSuppliers.SelectedItem);

            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lvSelectProductSupplier.SelectedItems == null)
            {
                MessageBox.Show("Please select one of the items in the box.");
            }
            //remove entries from listview
            else
            {
                foreach (ListViewItem item in lvSelectProductSupplier.SelectedItems)
                {
                    lvSelectProductSupplier.Items.Remove(item);
                }

                //get suppliers in listbox for a product
                List<TravelExperts.Mussie.Classes.Supplier> sup = new List<TravelExperts.Mussie.Classes.Supplier>();
                sup = PackagesDb.GetAllSupplier(Convert.ToInt16(cbProducts.SelectedValue));
                foreach (var item in sup)
                {
                    lbSuppliers.Items.Add(item);
                    lbSuppliers.DisplayMember = "SupName";
                }
            }
        }

        //save updated package to datatase
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (ValidData())
            {
                Packages Package = new Mussie.Classes.Packages();

                if (cbPackageName.SelectedText.ToString() != null)
                {
                    var package = (DataRowView)cbPackageName.SelectedItem;
                    string packageName = package["PkgName"].ToString();
                    int packageId = (int)package["PackageId"];
                    Package.PkgName = packageName;
                    Package.PackageID = packageId;
                }
                else
                {
                    MessageBox.Show("Please select a value.");
                }

                Package.PkgStartDate = dtpStartDate.Value;
                Package.PkgEndDate = dtpStartDate.Value;
                Package.PkgDesc = rtbPkgDesc.Text;

                //validate price
                if (ValidatorPackages.isEmpty(txtBasePrice))
                {
                    MessageBox.Show("please provide a Base Price.");
                }
                else
                {
                    Package.PkgBasePrice = ValidatorPackages.isDecimal(txtBasePrice);
                }

                Package.PkgAgencyCommission = Convert.ToDecimal(txtCommission.Text);

                int success = PackagesDb.updatePackage(Package);
                if (success >= 1)
                {
                    //update changes to Package_Product_Supplier table
                    foreach (ListViewItem item in lvSelectProductSupplier.Items)
                    {
                        int psi = Convert.ToInt16(item.SubItems[2].Text);
                        int result = PackagesDb.addProductSupplierToPackage(psi, Package.PackageID);
                    }
                    MessageBox.Show("You have successfully updated the package", "Success");
                }
                else
                {
                    MessageBox.Show("Package could not be updated", "Failure");
                }

                //populate listview for products
                lvPackageDetails.Items.Clear();
                List<PackageProductDetails> ppd = new List<PackageProductDetails>();
                ppd = PackagesDb.GetPackageProductDetails(Convert.ToInt16(cbPackageName.SelectedValue));
                foreach (var item in ppd)
                {
                    lvPackageDetails.Items.Add(new ListViewItem(new string[] { item.prodName, item.supplierName }));
                }
                lvSelectProductSupplier.Items.Clear();
            }
        }


        private bool ValidData()
        {
            return
            Validator.ValidDate(dtpStartDate.Value, dtpEndDate.Value) &&
            Validator.IsPresent(rtbPkgDesc) &&
            Validator.IsPresent(txtBasePrice) &&
            Validator.IsDecimal(txtBasePrice) &&
            Validator.IsWithinRange(txtBasePrice, 0, 10000000) &&
            Validator.IsPresent(txtCommission) &&
            Validator.IsDecimal(txtCommission) &&
            Validator.IsWithinRange(txtCommission, 0, 10000000) &&
            Validator.CommissionBasePriceCheck(Convert.ToDecimal(txtBasePrice.Text), Convert.ToDecimal(txtCommission.Text));

        }

        private void cbPackageName_SelectedIndexChanged(object sender, EventArgs e)
        {
            Packages pack = null;
            string ID = cbPackageName.SelectedValue.ToString();
            var package = (DataRowView)cbPackageName.SelectedItem;
            string packageName = package["PkgName"].ToString();
            int packageId = (int)package["PackageId"];

            pack = PackagesDb.getPackage(packageId);
            dtpStartDate.Value = pack.PkgStartDate;
            dtpEndDate.Value = pack.PkgEndDate;
            rtbPkgDesc.Text = pack.PkgDesc;
            txtBasePrice.Text = pack.PkgBasePrice.ToString();
            txtCommission.Text = pack.PkgAgencyCommission.ToString();

            //get products in to listbox
            lvPackageDetails.Items.Clear();
            if (cbPackageName.Focused == true)
            {
                List<PackageProductDetails> ppd = new List<PackageProductDetails>();
                ppd = PackagesDb.GetPackageProductDetails(packageId);
                foreach (var item in ppd)
                {
                    lvPackageDetails.Items.Add(new ListViewItem(new string[] { item.prodName, item.supplierName }));
                }
                lvPackageDetails.Refresh();
            }
            lvSelectProductSupplier.Items.Clear();
        }

        private void cbProducts_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbProducts.Focused == true)
            {
                var product = (DataRowView)cbPackageName.SelectedItem;
                int temp = Convert.ToInt16(cbProducts.SelectedValue);
                List<TravelExperts.Mussie.Classes.Supplier> sup = new List<TravelExperts.Mussie.Classes.Supplier>();

                sup = PackagesDb.GetAllSupplier(temp);
                lbSuppliers.Items.Clear();
                foreach (var item in sup)
                {
                    lbSuppliers.Items.Add(item);
                    lbSuppliers.DisplayMember = "SupName";
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
