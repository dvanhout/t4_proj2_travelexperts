﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


/*
 * Author: Mussie Tekeste
 * Date: July, 2017
 * Description:  
 */

namespace TravelExperts
{
    //validator class for creating and updating packages
    class ValidatorPackages
    {
        public static bool validateDates(DateTime startDate, DateTime endDate)
        {

            if (startDate < endDate)
            {
                return true;
            }
            else
            {
                MessageBox.Show("Package End date must be greater than the start date.");
                return false;
            }
        }

        public static bool isEmpty(TextBox tb)
        {
            if (tb.Text == "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static decimal isDecimal(TextBox tb)
        {
            decimal price;
            decimal cost = 0;
            if (decimal.TryParse(tb.Text, out price))
            {
                cost = price;
                return cost;
            }
            else
            {
                MessageBox.Show("The price value has to be decimal.");
                return price;
            }
        }

        public static bool IsDateProvided(DateTime date)
        {
            if (date.Date < DateTime.Now.Date)
            {
                MessageBox.Show("Date provided should be in the future from today.");
                return false;
            }
            else
            {
                return true;
            }
        }
        public static bool IsValidData(TextBox tb,
            DateTime start,DateTime end,TextBox basePrice) {
            if (!isEmpty(tb)) {
                if (IsDateProvided(start))
                {
                    if (IsDateProvided(end))
                    {
                        if (validateDates(start, end))
                        {
                            if (!isEmpty(basePrice))
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
            else {

                return false;
            }           
        }  
    }
}
