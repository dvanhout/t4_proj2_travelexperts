﻿
/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  Package class
 *            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts {
    public class Package {
        public int PackageId { get; set; }
        public string PkgName { get; set; }
        public DateTime PkgStartDate { get; set; }
        public DateTime PkgEndDate { get; set; }
        public string PkgDesc { get; set; }
        public decimal PkgBasePrice { get; set; }
        public decimal PkgAgencyCommission { get; set; }

        // two constructors - with and without data
        public Package() { }

        public Package(int pkgId, string pkgName, DateTime pkgStartDate, DateTime pkgEndDate, 
                       string pkgDesc, decimal pkgBasePrice, decimal pkgAgencyCommission) {
            PackageId = pkgId;
            PkgName = pkgName;
            PkgStartDate = pkgStartDate;
            PkgEndDate = pkgEndDate;
            PkgDesc = pkgDesc;
            PkgBasePrice = pkgBasePrice;
            PkgAgencyCommission = pkgAgencyCommission;
        }
    }
}
