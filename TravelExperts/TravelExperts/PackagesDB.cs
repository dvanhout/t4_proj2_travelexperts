﻿/*
 * Author:  Don van Hout
 * Date: July, 2017
 * Description:  Database methods for selecting, inserting, updating, deleting
 *              from packages table
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts {
    public static class PackagesDB {
        public static List<Package> GetAllPackages() {
            List<Package> packages = new List<Package>();
            Package pack = null;
            SqlConnection con = TravelExpertsDB.GetConnection();

            string selectStatement = "SELECT PackageId, PkgName, PkgStartDate, " +
                                     "       PkgEndDate, PkgDesc, PkgBasePrice, PkgAgencyCommission " +
                                     "FROM Packages";
            SqlCommand selectCommand = new SqlCommand(selectStatement, con);

            try {
                con.Open();
                SqlDataReader pkgsReader = selectCommand.ExecuteReader(); // read data into list
                while (pkgsReader.Read()) {
                    pack = new Package();
                    pack.PackageId = (int)pkgsReader["PackageId"];
                    pack.PkgName = pkgsReader["PkgName"].ToString();
                    //if (pkgsReader["PkgStartDate"] == DBNull.Value) {
                        //pack.PkgStartDate = null;
                    //} else {
                        pack.PkgStartDate = (DateTime)pkgsReader["PkgStartDate"];
                    //}

                    //if (pkgsReader["PkgEndDate"] == DBNull.Value) {
                    //    pack.PkgEndDate = null;
                   // } else { 
                        pack.PkgEndDate= (DateTime)pkgsReader["PkgEndDate"];
                   // }

                    pack.PkgDesc = pkgsReader["PkgDesc"].ToString();
                    pack.PkgBasePrice = (decimal)pkgsReader["PkgBasePrice"];

                //   if (pkgsReader["PkgAgencyCommission"] == DBNull.Value) {
                 //       pack.PkgAgencyCommission = null;
                //    } else {
                        pack.PkgAgencyCommission = (decimal)pkgsReader["PkgAgencyCommission"];
                //    }
                    packages.Add(pack); // add to list
                }
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
            return packages;
        }


        public static Package GetPackage(int packageId) {
            Package pack = null;
            SqlConnection con = TravelExpertsDB.GetConnection();

            string selectStatement = "SELECT PackageId, PkgName, PkgStartDate, " +
                                     "       PkgEndDate, PkgDesc, PkgBasePrice, PkgAgencyCommission " +
                                     "FROM Packages " +
                                     "WHERE PackageId = @PackageId";
            SqlCommand selectCommand = new SqlCommand(selectStatement, con);
            selectCommand.Parameters.AddWithValue("@PackageId", packageId);
            try {
                con.Open();
                SqlDataReader pkgsReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow);
                if (pkgsReader.Read()) {
                    pack = new Package();
                    pack.PackageId = (int)pkgsReader["PackageId"];
                    pack.PkgName = pkgsReader["PkgName"].ToString();
                    //if (pkgsReader["PkgStartDate"] == DBNull.Value) {
                   //     pack.PkgStartDate = null;
                   // } else {
                        pack.PkgStartDate = (DateTime)pkgsReader["PkgStartDate"];
                   // }

                    //if (pkgsReader["PkgEndDate"] == DBNull.Value) {
                   //     pack.PkgEndDate = null;
                   // } else {
                        pack.PkgEndDate = (DateTime)pkgsReader["PkgEndDate"];
                   // }

                    pack.PkgDesc = pkgsReader["PkgDesc"].ToString();
                    pack.PkgBasePrice = (decimal)pkgsReader["PkgBasePrice"];

                   // if (pkgsReader["PkgAgencyCommission"] == DBNull.Value) {
                    //    pack.PkgAgencyCommission = null;
                    //} else {
                        pack.PkgAgencyCommission = (decimal)pkgsReader["PkgAgencyCommission"];
                //    }
                    return pack;
                } else { return null; }
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
        }


        public static int AddPackage(Package package) {
            SqlConnection con = TravelExpertsDB.GetConnection();

            string insertStatement = "INSERT INTO Packages " +
                                     "(PkgName, PkgStartDate, " +
                                     "PkgEndDate, PkgDesc, PkgBasePrice, PkgAgencyCommission) " +
                                     "VALUES (@PkgName, @PkgStartDate, " +
                                     "       @PkgEndDate, @PkgDesc, @PkgBasePrice, @PkgAgencyCommission)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, con);
            //insertCommand.Parameters.AddWithValue("@PackageId", package.PackageId);
            insertCommand.Parameters.AddWithValue("@PkgName", package.PkgName);
            insertCommand.Parameters.AddWithValue("@PkgStartDate", (object)package.PkgStartDate);
            insertCommand.Parameters.AddWithValue("@PkgEndDate", (object)package.PkgEndDate);
            insertCommand.Parameters.AddWithValue("@PkgDesc", package.PkgDesc);
            insertCommand.Parameters.AddWithValue("@PkgBasePrice", package.PkgBasePrice);
            insertCommand.Parameters.AddWithValue("@PkgAgencyCommission", (object)package.PkgAgencyCommission);
            try {
                con.Open();
                int count = insertCommand.ExecuteNonQuery();
                string selectStatement = "SELECT IDENT_CURRENT('Packages') from Packages";
                SqlCommand selectCommand = new SqlCommand(selectStatement, con);
                int packageID = Convert.ToInt32(selectCommand.ExecuteScalar());
                return packageID;
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
        }


        public static bool UpdatePackage(int packageId, Package newPackage) {
            SqlConnection con = TravelExpertsDB.GetConnection();

            string updateStatement = "UPDATE Packages " +
                                     "SET PkgName = @newPackageName, " +
                                     "PkgStartDate = @newPkgStartDate, PkgEndDate = @newPkgEndDate, " +
                                     "PkgDesc = @newPkgDesc, PkgBasePrice = @newPkgBasePrice, " +
                                     "PkgAgencyCommission = @newPkgAgencyCommission " +
                                     "WHERE PackageId = @PackageId";
            SqlCommand updateCommand = new SqlCommand(updateStatement, con);
            updateCommand.Parameters.AddWithValue("@newPackageName", newPackage.PkgName);
            updateCommand.Parameters.AddWithValue("@newPkgStartDate", (object)newPackage.PkgStartDate);
            updateCommand.Parameters.AddWithValue("@newPkgEndDate", (object)newPackage.PkgEndDate);
            updateCommand.Parameters.AddWithValue("@newPkgDesc", newPackage.PkgDesc);
            updateCommand.Parameters.AddWithValue("@newPkgBasePrice", newPackage.PkgBasePrice);
            updateCommand.Parameters.AddWithValue("@newPkgAgencyCommission", (object)newPackage.PkgAgencyCommission);
            updateCommand.Parameters.AddWithValue("@PackageID", packageId);

            try {
                con.Open();
                int count = updateCommand.ExecuteNonQuery();
                if (count > 0) return true;
                else return false;
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
        }

        public static bool DeletePackage(int packageId) {
            SqlConnection con = TravelExpertsDB.GetConnection();

            string deleteStatement = "DELETE FROM Packages " +
                                     "WHERE PackageId = @PackageId";
            SqlCommand deleteCommand = new SqlCommand(deleteStatement, con);
            deleteCommand.Parameters.AddWithValue("@PackageId", packageId);
            try {
                con.Open();
                int count = deleteCommand.ExecuteNonQuery();
                if (count > 0) return true;
                else return false;
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
        }

    }
}
