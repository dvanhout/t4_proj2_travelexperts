﻿/*
 * Author: 
 * Date: July, 2017
 * Description:  Defines Packages_Products_Suppliers object
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts {
    public class Packages_Products_Suppliers {
        // properties
        public int PackageID { get; set; }
        public int ProductSupplierID { get; set; }

        // empty constructor
        public Packages_Products_Suppliers() { }

        // constructor with properties
        public Packages_Products_Suppliers(int pkgID, int prodSupID) {
            PackageID = pkgID;
            ProductSupplierID = prodSupID;
        }
    }
}
