﻿/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  Database methods for retrieving, inserting, updating, and deleting records 
 *               from the Packages_Products_Suppliers table
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts {
    public static class Packages_Products_SuppliersDB {
        // retrieves and returns a list of all Packages_Products_Suppliers from database
        public static List<Packages_Products_Suppliers> GetAllPackProdSup() {
            List<Packages_Products_Suppliers> packProdSupList = new List<Packages_Products_Suppliers>();
            Packages_Products_Suppliers packProdSup;
            SqlConnection con = TravelExpertsDB.GetConnection();

            string selectStatement = "SELECT PackageId, ProductSupplierId " +
                                     "FROM Packages_Products_Suppliers";
            SqlCommand selectCommand = new SqlCommand(selectStatement, con);

            try {
                con.Open();
                SqlDataReader ppsReader = selectCommand.ExecuteReader(); // read data into list
                while (ppsReader.Read()) {
                    packProdSup = new Packages_Products_Suppliers((int)ppsReader["PackageId"], 
                                                                  (int)ppsReader["ProductSupplierId"]);
                    packProdSupList.Add(packProdSup); // add to list
                }
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
            return packProdSupList;
        }

        // returns a list of packages_products_suppliers by package id
        public static List<Packages_Products_Suppliers> GetPackProdSupsByPackId(int packageId) {
            List<Packages_Products_Suppliers> packProdSupList = new List<Packages_Products_Suppliers>();
            Packages_Products_Suppliers packProdSup;
            SqlConnection con = TravelExpertsDB.GetConnection();

            string selectStatement = "SELECT PackageId, ProductSupplierId " +
                                     "FROM Packages_Products_Suppliers " +
                                     "WHERE PackageId = @PackageId";
            SqlCommand selectCommand = new SqlCommand(selectStatement, con);
            selectCommand.Parameters.AddWithValue("@PackageId", packageId);
            try {
                con.Open();
                SqlDataReader ppsReader = selectCommand.ExecuteReader(); // read data into list
                while (ppsReader.Read()) {
                    packProdSup = new Packages_Products_Suppliers((int)ppsReader["PackageId"],
                                                                  (int)ppsReader["ProductSupplierId"]);
                    packProdSupList.Add(packProdSup); // add to list
                }
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
            return packProdSupList;
        }


        // retrieves and returns a single Pacages_Products_Suppliers record from database based on ProductSupplierId
        public static Packages_Products_Suppliers GetPackProdSup(int packId, int prodSupId) {
            Packages_Products_Suppliers packProdSup;
            SqlConnection con = TravelExpertsDB.GetConnection();

            string selectStatement = "SELECT PackageId, ProductSupplierId " +
                                     "FROM Packages_Products_Suppliers " +
                                     "WHERE PackageId = @PackageId AND ProductSupplierId = @ProductSupplierId";
            SqlCommand selectCommand = new SqlCommand(selectStatement, con);
            selectCommand.Parameters.AddWithValue("@PackageId", packId);
            selectCommand.Parameters.AddWithValue("@ProductSupplierId", prodSupId);

            try {
                con.Open();
                SqlDataReader ppsReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow);
                if (ppsReader.Read()) {
                    packProdSup = new Packages_Products_Suppliers((int)ppsReader["PackageId"],
                                                     (int)ppsReader["ProductSupplierId"]);
                    return packProdSup;
                } else { return null; }
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
        }



        public static bool PackProdSupExists(int packageID, int productSupplierID) {
                  
            SqlConnection con = TravelExpertsDB.GetConnection();

            string selectStatement = "SELECT COUNT(*) FROM Packages_Products_Suppliers " +
                                     "WHERE PackageId=@PackageId AND ProductSupplierId = @ProductSupplierId";
            SqlCommand selectCommand = new SqlCommand(selectStatement, con);
            selectCommand.Parameters.AddWithValue("@PackageId", packageID);
            selectCommand.Parameters.AddWithValue("@ProductSupplierId", productSupplierID);

            try {
                con.Open();
                int count = (int)selectCommand.ExecuteScalar();
                if (count > 0) return true;
                else return false;
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
        }
    

        // inserts a new Packages_Products_Suppliers record into the database, return true/false on success/fail
        public static bool AddPackProdSup(Packages_Products_Suppliers packProdSup) {
            SqlConnection con = TravelExpertsDB.GetConnection();

            string insertStatement = "INSERT INTO Packages_Products_Suppliers " +
                                     "(PackageId, ProductSupplierId) " +
                                     "VALUES (@PackageId, @ProductSupplierId)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, con);
            insertCommand.Parameters.AddWithValue("@PackageId", packProdSup.PackageID);
            insertCommand.Parameters.AddWithValue("@ProductSupplierId", packProdSup.ProductSupplierID);

            try {
                con.Open();
                int count = insertCommand.ExecuteNonQuery();
                if (count > 0) return true;
                else return false;
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
        }


        // updates record in the database
        public static bool UpdatePackProdSup(Packages_Products_Suppliers oldPackProdSup, 
                                             Packages_Products_Suppliers newPackProdSup) {
            SqlConnection con = TravelExpertsDB.GetConnection();

            string updateStatement = "UPDATE Packages_Products_Suppliers " +
                                     "SET PackageId = '@newPackageId', ProductSupplierId = '@newProductSupplierId' " +
                                     "WHERE PackageId = @oldPackageId AND ProductSupplierId = @oldProductSupplierId";
            SqlCommand updateCommand = new SqlCommand(updateStatement, con);
            updateCommand.Parameters.AddWithValue("@newPackageId", newPackProdSup.PackageID);
            updateCommand.Parameters.AddWithValue("@newProductSupplierId", newPackProdSup.ProductSupplierID);
            updateCommand.Parameters.AddWithValue("@oldPackageId", oldPackProdSup.PackageID);
            updateCommand.Parameters.AddWithValue("@oldProductSupplierId", oldPackProdSup.ProductSupplierID);
            try {
                con.Open();
                int count = updateCommand.ExecuteNonQuery();
                if (count > 0) return true;
                else return false;
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
        }


        // deletes a record from the database
        public static bool DeletePackProdSup(Packages_Products_Suppliers packProdSup) {
            SqlConnection con = TravelExpertsDB.GetConnection();

            string deleteStatement = "DELETE FROM Packages_Products_Suppliers " +
                                     "WHERE PackageId = @PackageId AND ProductSupplierId = @ProductSupplierId";
            SqlCommand deleteCommand = new SqlCommand(deleteStatement, con);
            deleteCommand.Parameters.AddWithValue("@PackageId", packProdSup.PackageID);
            deleteCommand.Parameters.AddWithValue("@ProductSupplierId", packProdSup.ProductSupplierID);
            try {
                con.Open();
                int count = deleteCommand.ExecuteNonQuery();
                if (count > 0) return true;
                else return false;
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
        }
    }
}
