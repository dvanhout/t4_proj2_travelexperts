﻿/*
 * Author: Dave Jookhuu
 * Date: July, 2017
 * Description:  
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts {
    public class Product {
        public int ProductID { get; set; }
        public string ProdName { get; set; }
        public Product() { }
    }
}
