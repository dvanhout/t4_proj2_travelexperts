﻿/*
 * Author: Dave Jookhuu 
 * Date: July, 2017
 * Description:  
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts {
    public class ProductsDB
    {
        //get all products
        public static List<Product> GetAllProducts()
        {
            //setting list that will store all he products
            List<Product> prodList = new List<Product>();
            Product NextProduct;

            //getting pruduct 
            SqlConnection con = TravelExpertsDB.GetConnection();
            string selectStatement = "SELECT ProductId, ProdName from Products ORDER BY ProductId";
            SqlCommand SelectCmd = new SqlCommand(selectStatement, con);
            try
            {
                con.Open();
                SqlDataReader reader = SelectCmd.ExecuteReader();
                while (reader.Read())
                {
                    NextProduct = new Product();
                    NextProduct.ProductID = (int)reader["ProductID"];
                    NextProduct.ProdName = (string)reader["ProdName"];
                    prodList.Add(NextProduct);
                }
            }
            catch (SqlException ex) { throw ex; }
            finally { con.Close(); }

            return prodList;
        }//end GetAllProducts()

        //getting a single product
        public static Product GetProduct(int ProductID)
        {
            Product tempProdHolder = new Product();

            SqlConnection conn = TravelExpertsDB.GetConnection();
            string selectStatement = "SELECT ProductID, ProdName FROM products WHERE ProductID=@ProductID";
            SqlCommand selectCmd = new SqlCommand(selectStatement, conn);
            selectCmd.Parameters.AddWithValue("@ProductID", ProductID);

            try
            {
                conn.Open();
                SqlDataReader reader = selectCmd.ExecuteReader();
                if (reader.Read())
                {
                    tempProdHolder.ProductID = (int)reader["ProductID"];
                    tempProdHolder.ProdName = (string)reader["ProdName"];
                }

                return tempProdHolder;

            }
            catch (SqlException ex)
            {

                throw ex;
            }
            finally
            {
                conn.Close();
            }

        }//end GetProduct()

        //updage product
        public static bool UpdateProduct(int productID, string prodName)
        {
            SqlConnection conn = TravelExpertsDB.GetConnection();
            string selectStatement = "UPDATE products SET ProdName=@prodName WHERE ProductID=@productID";
            SqlCommand selectCmd = new SqlCommand(selectStatement, conn);
            selectCmd.Parameters.AddWithValue("@prodName", prodName);
            selectCmd.Parameters.AddWithValue("@productID", productID);

            try
            {
                conn.Open();
                int count = selectCmd.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (DBConcurrencyException ex)
            {
                throw ex;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }

        }//end UpdateProduct()

        //delete product
        public static bool DeleteProduct(int ProductID)
        {
            SqlConnection conn = TravelExpertsDB.GetConnection();
            string selectStatement = "DELETE FROM products WHERE @ProductID=ProductID";
            SqlCommand selectCmd = new SqlCommand(selectStatement, conn);
            selectCmd.Parameters.AddWithValue("@ProductID", ProductID);

            try
            {
                conn.Open();
                int count = selectCmd.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {

                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }//end DeleteProducts

        //insert Product
        public static bool AddProduct(string ProdName)
        {
            SqlConnection conn = TravelExpertsDB.GetConnection();
            string selectStatement = "INSERT INTO products VALUES (@ProdName)";
            SqlCommand selectCommand = new SqlCommand(selectStatement, conn);
            selectCommand.Parameters.AddWithValue("@ProdName", ProdName);

            try
            {
                conn.Open();
                int count = selectCommand.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {

                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }//end AddProduct()

    }//end ProductsDB Class
}
