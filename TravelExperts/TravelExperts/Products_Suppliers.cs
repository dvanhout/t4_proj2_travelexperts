﻿/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  Defines Products_Suppliers object
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts {
    public class Products_Suppliers {
        // properties
        public int ProductSupplierID { get; set; }
        public int ProductID { get; set; }
        public int SupplierID { get; set; }


        // empty constructor
        public Products_Suppliers() { }

        // constructor with properties
        public Products_Suppliers(int prodSupID, int prodID, int supID) {
            ProductSupplierID = prodSupID;
            ProductID = prodID;
            SupplierID = supID;
        }

    }
}
