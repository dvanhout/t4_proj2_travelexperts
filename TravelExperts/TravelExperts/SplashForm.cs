﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TravelExperts.Properties;

namespace TravelExperts {
    public partial class SplashForm : Form {
        public SplashForm() {
            InitializeComponent();

            PictureBox spashPictureBox = new PictureBox();
            spashPictureBox.Image = Resources.te_SplashFormImage1;
            spashPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            spashPictureBox.Dock = DockStyle.Fill;
            this.Controls.Add(spashPictureBox);
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void timer1_Tick(object sender, EventArgs e) {
            progressBar1.Increment(1);
            if (progressBar1.Value == 100) {
                timer1.Stop();
            }
        }
    }
}
