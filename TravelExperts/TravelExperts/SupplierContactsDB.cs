﻿/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  Db access methods for retrieving, updating, inserting, and deleting records
 */
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts {
    public static class SupplierContactsDB {

        // returns list of all supplier contacts from database
        public static List<SupplierContact> GetAllSupContacts() {
            List<SupplierContact> supContacts = new List<SupplierContact>();
            SupplierContact supC;
            SqlConnection con = TravelExpertsDB.GetConnection();

            string selectStatement = "SELECT SupplierContactId,  SupConFirstName, " +
                                     "SupConLastName, SupConCompany, SupConAddress, " +
                                     "SupConCity, SupConProv, SupConPostal, " +
                                     "SupConCountry, SupConBusPhone, SupConFax, " +
                                     "SupConEmail, SupConURL, AffiliationId, " +
                                     "SupplierId " +
                                     "FROM SupplierContacts";
            SqlCommand selectCommand = new SqlCommand(selectStatement, con);

            try {
                con.Open();
                SqlDataReader scReader = selectCommand.ExecuteReader(); // read data into list
                while (scReader.Read()) {
                    supC = new SupplierContact();
                    supC.SupplierContactId = (int)scReader["SupplierContactId"];
                    supC.SupConFirstName = scReader["SupConFirstName"].ToString();
                    supC.SupConLastName = scReader["SupConLastName"].ToString();
                    supC.SupConCompany = scReader["SupConCompany"].ToString();
                    supC.SupConAddress = scReader["SupConAddress"].ToString();
                    supC.SupConCity = scReader["SupConCity"].ToString();
                    supC.SupConProv = scReader["SupConProv"].ToString();
                    supC.SupConPostal = scReader["SupConPostal"].ToString();
                    supC.SupConCountry = scReader["SupConCountry"].ToString();
                    supC.SupConBusPhone = scReader["SupConBusPhone"].ToString();
                    supC.SupConFax = scReader["SupConFax"].ToString();
                    supC.SupConEmail = scReader["SupConEmail"].ToString();
                    supC.SupConURL = scReader["SupConURL"].ToString();
                    supC.AffiliationId = scReader["AffiliationId"].ToString();
                    supC.SupplierId = (int)scReader["SupplierId"];

                    supContacts.Add(supC); // add to list
                }
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
            return supContacts;
        }


        // returns single supplier contact record based on id
        public static SupplierContact GetSupCon(int supplierContactId) {
            SupplierContact supC = new SupplierContact();
            SqlConnection con = TravelExpertsDB.GetConnection();

            string selectStatement = "SELECT SupplierContactId,  SupConFirstName, " +
                                     "SupConLastName, SupConCompany, SupConAddress, " +
                                     "SupConCity, SupConProv, SupConPostal, " +
                                     "SupConCountry, SupConBusPhone, SupConFax, " +
                                     "SupConEmail, SupConURL, AffiliationId, " +
                                     "SupplierId " +
                                     "FROM SupplierContacts " +
                                     "WHERE SupplierContactID = @SupplierContactId";
            SqlCommand selectCommand = new SqlCommand(selectStatement, con);
            selectCommand.Parameters.AddWithValue("@SupplierContactId", supplierContactId);
            try {
                con.Open();
                SqlDataReader scReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow);
                if (scReader.Read()) {
                    supC.SupplierContactId = (int)scReader["SupplierContactId"];
                    if (scReader["SupConFirstName"] == DBNull.Value)
                        supC.SupConFirstName = null;
                    else
                        supC.SupConFirstName = scReader["SupConFirstName"].ToString();

                    if (scReader["SupConLastName"] == DBNull.Value)
                        supC.SupConLastName = null;
                    else
                        supC.SupConLastName = scReader["SupConLastName"].ToString();

                    if (scReader["SupConCompany"] == DBNull.Value)
                        supC.SupConCompany = null;
                    else
                        supC.SupConCompany = scReader["SupConCompany"].ToString();

                    if (scReader["SupConAddress"] == DBNull.Value)
                        supC.SupConAddress = null;
                    else
                        supC.SupConAddress = scReader["SupConAddress"].ToString();

                    if (scReader["SupConCity"] == DBNull.Value)
                        supC.SupConCity = null;
                    else
                        supC.SupConCity = scReader["SupConCity"].ToString();

                    if (scReader["SupConProv"] == DBNull.Value)
                        supC.SupConProv = null;
                    else
                        supC.SupConProv = scReader["SupConProv"].ToString();
                    if (scReader["SupConPostal"] == DBNull.Value)
                        supC.SupConPostal = null;
                    else
                        supC.SupConPostal = scReader["SupConPostal"].ToString();
                    if (scReader["SupConCountry"] == DBNull.Value)
                        supC.SupConCountry = null;
                    else
                        supC.SupConCountry = scReader["SupConCountry"].ToString();
                    if (scReader["SupConBusPhone"] == DBNull.Value)
                        supC.SupConBusPhone = null;
                    else
                        supC.SupConBusPhone = scReader["SupConBusPhone"].ToString();
                    if (scReader["SupConFax"] == DBNull.Value)
                        supC.SupConFax = null;
                    else
                        supC.SupConFax = scReader["SupConFax"].ToString();
                    if (scReader["SupConEmail"] == DBNull.Value)
                        supC.SupConEmail = null;
                    else
                        supC.SupConEmail = scReader["SupConEmail"].ToString();
                    if (scReader["SupConURL"] == DBNull.Value)
                        supC.SupConURL = null;
                    else
                        supC.SupConURL = scReader["SupConURL"].ToString();
                    if (scReader["AffiliationId"] == DBNull.Value)
                        supC.AffiliationId = null;
                    else
                        supC.AffiliationId = scReader["AffiliationId"].ToString();

                    supC.SupplierId = (int)scReader["SupplierId"];

                    return supC;
                } else { return null; }
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
        }


        // adds new supplier contact record to db
        public static int AddSupCon(SupplierContact supplierContact) {
            SqlConnection con = TravelExpertsDB.GetConnection();

            string insertStatement = "INSERT INTO SupplierContacts " +
                                     "(SupplierContactId, SupConFirstName, " +
                                     "SupConLastName, SupConCompany, SupConAddress, " +
                                     "SupConCity, SupConProv, SupConPostal, " +
                                     "SupConCountry, SupConBusPhone, SupConFax, " +
                                     "SupConEmail, SupConURL, " +
                                     "SupplierId) " +
                                     "VALUES ((SELECT MAX(SupplierContactId) FROM SupplierContacts) + 1, " +
                                     "        @SupConFirstName, @SupConLastName, " +
                                     "        @SupConCompany, @SupConAddress, @SupConCity, " +
                                     "        @SupConProv, @SupConPostal, @SupConCountry, " +
                                     "        @SupConBusPhone, @SupConFax, @SupConEmail, " +
                                     "        @SupConURL, @SupplierId)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, con);
            insertCommand.Parameters.AddWithValue("@SupConFirstName", supplierContact.SupConFirstName);
            insertCommand.Parameters.AddWithValue("@SupConLastName", supplierContact.SupConLastName);
            insertCommand.Parameters.AddWithValue("@SupConCompany", supplierContact.SupConCompany);
            insertCommand.Parameters.AddWithValue("@SupConAddress", supplierContact.SupConAddress);
            insertCommand.Parameters.AddWithValue("@SupConCity", supplierContact.SupConCity);
            insertCommand.Parameters.AddWithValue("@SupConProv", supplierContact.SupConProv);
            insertCommand.Parameters.AddWithValue("@SupConPostal", supplierContact.SupConPostal);
            insertCommand.Parameters.AddWithValue("@SupConCountry", supplierContact.SupConCountry);
            insertCommand.Parameters.AddWithValue("@SupConBusPhone", supplierContact.SupConBusPhone);
            insertCommand.Parameters.AddWithValue("@SupConFax", supplierContact.SupConFax);
            insertCommand.Parameters.AddWithValue("@SupConEmail", supplierContact.SupConEmail);
            insertCommand.Parameters.AddWithValue("@SupConURL", supplierContact.SupConURL);
            // insertCommand.Parameters.AddWithValue("@AffiliationId", (object)supplierContact.AffiliationId ?? DBNull.Value);
            insertCommand.Parameters.AddWithValue("@SupplierId", (object)supplierContact.SupplierId ?? 0);
            try {
                con.Open();
                int count = insertCommand.ExecuteNonQuery();
                string selectStatement = "SELECT MAX(SupplierContactId) AS SupplierContact FROM SupplierContacts";
                SqlCommand selectCommand = new SqlCommand(selectStatement, con);
                int supConID = 0;
                SqlDataReader scReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow);
                if (scReader.Read()) {
                    supConID = Convert.ToInt32(scReader["SupplierContact"]);
                }
                return supConID;              
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
        }


        // updates supplier contact based on supplier id and newly passed suppliercontact
        public static bool UpdateSupCon(int supplierContactId, SupplierContact newSupplierContact) {
            SqlConnection con = TravelExpertsDB.GetConnection();

            string updateStatement = "UPDATE SupplierContacts " +
                                     "SET SupConFirstName = @SupConFirstName, " +
                                     "    SupConLastName = @SupConLastName, " +
                                     "    SupConCompany = @SupConCompany, " +
                                     "    SupConAddress = @SupConAddress, " +
                                     "    SupConCity = @SupConCity, " +
                                     "    SupConProv = @SupConProv, " +
                                     "    SupConPostal = @SupConPostal, " +
                                     "    SupConCountry = @SupConCountry, " +
                                     "    SupConBusPhone = @SupConBusPhone, " +
                                     "    SupConFax = @SupConFax, " +
                                     "    SupConEmail = @SupConEmail, " +
                                     "    SupConURL = @SupConURL, " +
                                     "    SupplierId = @SupplierId " +
                                     "WHERE SupplierContactId = @SupplierContactId";
            SqlCommand updateCommand = new SqlCommand(updateStatement, con);
            updateCommand.Parameters.AddWithValue("@SupConFirstName", newSupplierContact.SupConFirstName);
            updateCommand.Parameters.AddWithValue("@SupConLastName", newSupplierContact.SupConLastName);
            updateCommand.Parameters.AddWithValue("@SupConCompany", newSupplierContact.SupConCompany);
            updateCommand.Parameters.AddWithValue("@SupConAddress", newSupplierContact.SupConAddress);
            updateCommand.Parameters.AddWithValue("@SupConCity", newSupplierContact.SupConCity);
            updateCommand.Parameters.AddWithValue("@SupConProv", newSupplierContact.SupConProv);
            updateCommand.Parameters.AddWithValue("@SupConPostal", newSupplierContact.SupConPostal);
            updateCommand.Parameters.AddWithValue("@SupConCountry", newSupplierContact.SupConCountry);
            updateCommand.Parameters.AddWithValue("@SupConBusPhone", newSupplierContact.SupConBusPhone);
            updateCommand.Parameters.AddWithValue("@SupConFax", newSupplierContact.SupConFax);
            updateCommand.Parameters.AddWithValue("@SupConEmail", newSupplierContact.SupConEmail);
            updateCommand.Parameters.AddWithValue("@SupConURL", newSupplierContact.SupConURL);
            updateCommand.Parameters.AddWithValue("@SupplierId", newSupplierContact.SupplierId);
            updateCommand.Parameters.AddWithValue("@SupplierContactId", supplierContactId);

            try {
                con.Open();
                int count = updateCommand.ExecuteNonQuery();
                if (count > 0) return true;
                else return false;
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
        }


        // deletes supplierContact record from database based on id
        public static bool DeleteSupCon(int supplierContactId) {
            SqlConnection con = TravelExpertsDB.GetConnection();

            string deleteStatement = "DELETE FROM SupplierContacts " +
                                     "WHERE SupplierContactId = @SupplierContactId";
            SqlCommand deleteCommand = new SqlCommand(deleteStatement, con);
            deleteCommand.Parameters.AddWithValue("@SupplierContactId", supplierContactId);
            try {
                con.Open();
                int count = deleteCommand.ExecuteNonQuery();
                if (count > 0) return true;
                else return false;
            } catch (SqlException ex) { throw ex; } finally { con.Close(); }
        }
    }
}
