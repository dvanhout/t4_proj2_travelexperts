﻿/*
 * Author: FABIAN ISAZA
 * Date: July 04, 2017
 * Description: SuppliersDB 
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts
{
    public class SuppliersDB
    {
        // Pull data from Database from Suppliers Table to put it into a list for comboBox
        public static List<Supplier> GetAllSuppliers()
        {
            List<Supplier> supplierIDs = new List<Supplier>();//Empty List
            Supplier nextSupplierID; //for reference

            SqlConnection con = TravelExpertsDB.GetConnection();
            string selectStatement = "SELECT SupplierID, SupName " +
                                     "FROM Suppliers ORDER BY SupName";
                                     //"WHERE SupplierId = @SupplierId";
            SqlCommand selectCommand = new SqlCommand(selectStatement, con);
            
            try
            {
                con.Open();
                SqlDataReader reader = selectCommand.ExecuteReader();
                while (reader.Read())
                {
                    nextSupplierID = new Supplier();
                    nextSupplierID.SupName = reader["SupName"].ToString();
                    nextSupplierID.SupplierID = Convert.ToInt32(reader["SupplierID"]);
                    supplierIDs.Add(nextSupplierID);
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return supplierIDs;
        }

        // Get every supplier ID and Name
        public static Supplier GetSupplier(int supplierID)
        {
            SqlConnection con = TravelExpertsDB.GetConnection();
            string selectStatement = "SELECT SupplierID, SupName " +
                                     "FROM Suppliers " +
                                     "WHERE SupplierID = @SupplierID";

            SqlCommand selectCommand = new SqlCommand(selectStatement, con);
            selectCommand.Parameters.AddWithValue("@SupplierID", supplierID);

            try
            {
                con.Open();
                SqlDataReader Reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow);
                if (Reader.Read())
                {
                    Supplier supplier = new Supplier();
                    supplier.SupplierID = (int)Reader["SupplierID"];
                    supplier.SupName = Reader["SupName"].ToString();
                    return supplier;
                }
                else
                {
                    return null;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        //Method to Add a supplier
        public static bool AddSupplier(string supName)
        {
            SqlConnection con = TravelExpertsDB.GetConnection();

            string insertStatement = "INSERT INTO Suppliers (SupplierId, supname) " +
                                     "VALUES ((SELECT MAX(SupplierId) + 1 FROM suppliers), @supName)";

            SqlCommand addCommand = new SqlCommand(insertStatement, con);
            //addCommand.Parameters.AddWithValue("@SupplierID", supplier.SupplierID);
            addCommand.Parameters.AddWithValue("@SupName", supName);

            try {
                con.Open();
                int count = addCommand.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            } catch (SqlException ex) {

                throw ex;
            } finally {
                con.Close();
            }
        }

        // Method to Update Supplier
        public static bool UpdateSupplier(Supplier oldSupplier, Supplier newSupplier)
        {
            SqlConnection con = TravelExpertsDB.GetConnection();

            string updateStatement = "UPDATE Suppliers " +
                                     "SET SupplierID = @newSupplierID, SupName = @newSupName " +
                                     "WHERE SupplierID = @oldSupplierID";

            SqlCommand updateCommand = new SqlCommand(updateStatement, con);
            updateCommand.Parameters.AddWithValue("@newSupplierID", newSupplier.SupplierID);
            updateCommand.Parameters.AddWithValue("@newSupName", newSupplier.SupName);
            updateCommand.Parameters.AddWithValue("@oldSupplierID", oldSupplier.SupplierID);

            try
            {
                con.Open();
                int count = updateCommand.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        // Method to Delete Supplier
        public static bool DeleteSupplier(Supplier sup)
        {
            SqlConnection con = TravelExpertsDB.GetConnection();
            string deleteStatement = "DELETE FROM Suppliers " + 
                                     "WHERE SupplierID = @SupplierID";

            SqlCommand deleteCommand = new SqlCommand(deleteStatement, con);
            deleteCommand.Parameters.AddWithValue("@SupplierID", sup.SupplierID);

            try
            {
                con.Open();
                int count = deleteCommand.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
