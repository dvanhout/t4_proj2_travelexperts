﻿/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  Method to get connection to the database
 */

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExperts {
    public static class TravelExpertsDB {

        // get connection from the Database
        public static SqlConnection GetConnection() {
            string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\TravelExperts.mdf;Integrated Security=True;Connect Timeout=30";
            SqlConnection con = new SqlConnection(connectionString);
            return con;
        }
    }
}
