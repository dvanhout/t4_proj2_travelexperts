﻿/*
 * Author: Mussie, Don 
 * Date: July, 2017
 * Description:  Validates form fields
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelExperts {
    public static class Validator
    {
        public static bool IsPresent(TextBox textBox, string name)
        {
            if (textBox.Text == "")
            {
                MessageBox.Show(name + " is a required field.", "Entry Error");
                textBox.Focus();
                return false;
            }
            return true;
        }

        public static bool isEmpty(TextBox tb)
        {

            if (tb.Text == "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool userName(TextBox tb)
        {
            var regex = "^[0-9]+$";//data can only have numbers in it. Validates AgentID from agents table
            var match = Regex.Match(tb.Text, regex, RegexOptions.IgnoreCase);
            if (!match.Success)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool Password(TextBox tb)
        {
            //^ ([2 - 9]\\d{ 2})\\d{ 3}
            //-\\d{ 4}$
            var regex = "^[0-9]+$";//data can only have numbers in it.
            var match = Regex.Match(tb.Text, regex, RegexOptions.IgnoreCase);
            if (!match.Success)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        public static bool IsPresent(TextBox tb)
        {
            if (tb.Text != "")
            {
                return true;
            }
            else
            {
                MessageBox.Show("Expected " + tb.Tag + " is required", "Input Error");
                tb.Focus();
                tb.BackColor = Color.FromArgb(255, 127, 80);
                return false;
            }
        }


        public static bool IsPresent(RichTextBox tb)
        {
            if (tb.Text != "")
            {
                return true;
            }
            else
            {
                MessageBox.Show("Expected " + tb.Tag + " is required", "Input Error");
                tb.Focus();
                tb.BackColor = Color.FromArgb(255, 127, 80);
                return false;
            }
        }


        public static bool IsWithinRange(TextBox tb, decimal min, decimal max) //test to see if it is in range  min and max
        {

            decimal value = Convert.ToDecimal(tb.Text); //integer value otherwise produces an error 
            if (value >= min && value <= max) //if between the max/min values return true
            {
                return true;
            }
            else
            {
                MessageBox.Show(tb.Tag + " Must be in range from " + min + " & " + max, "Input Error");
                tb.Focus();
                return false;
            }
        }



        public static bool IsInteger(TextBox tb)
        {
            int num; //auxillary for trying to parse
            if (Int32.TryParse(tb.Text, out num))
            {
                return true;
            }
            else
            {
                MessageBox.Show("Expected" + tb.Tag + " has to be a whole number", "Input Error");
                tb.Focus();
                return false;
            }
        }


        public static bool ValidDate(DateTime StartDate, DateTime EndDate)
        {
            if (EndDate.Date < StartDate.Date)
            {
                MessageBox.Show("End date has to be after the start date.", "Input Error");
                return false;
            }
            else
                return true;
        }



        public static bool IsDecimal(TextBox tb)
        {


            decimal num;
            if (decimal.TryParse(tb.Text.Replace("$", ""), out num))
            {
                return true;
            }
            else
            {

                MessageBox.Show(tb.Tag + " has to be a number!", "Input Error");
                tb.Focus();
                tb.BackColor = Color.FromArgb(255, 127, 80);
                return false;
            }

        }//isDecimal


        public static bool CommissionBasePriceCheck(decimal BasePrice, decimal Commission)
        {
            if (BasePrice < Commission)
            {
                MessageBox.Show("Base price has to be greater than the commission.", "Input Error");
                return false;
            }
            else
                return true;
        }


        // Don - validate currency fields to make sure it's positive
        public static bool IsPositiveCurrency(TextBox currencyTextBox, string msg) {
            if (decimal.Parse(currencyTextBox.Text, NumberStyles.Currency) >= 0) {
                return true;
            } else {
                MessageBox.Show(msg);
                return false;
            }
        }

        // Don - check for valid email 
        public static bool Email(TextBox tb, string message) {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(tb.Text);
            if (match.Success) return true;
            else {
                MessageBox.Show(message);
                return false;
            }
        }

        // Don -  check for valid north american phone number
        public static bool Phone(MaskedTextBox mtb, string message) {
            Regex regex = new Regex("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$");
            Match match = regex.Match(mtb.Text);
            if (match.Success) return true;
            else {
                MessageBox.Show(message);
                return false;
            }
        }
        
        // Don - check for valid url
        public static bool URL(TextBox tb, string message) {
            if (Uri.IsWellFormedUriString(tb.Text, UriKind.RelativeOrAbsolute))
                return true;
            else {
                MessageBox.Show(message);
                return false;
            }
        }

        // Don - check for valid canadian and american postal codes
        public static bool Postal(TextBox tb, string message) {
            string usRegex = @"^\d{5}(?:[-\s]\d{4})?$";
            string caRegex = @"^([ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ])\ {0,1}(\d[ABCEGHJKLMNPRSTVWXYZ]\d)$";

            Regex usZipRegex = new Regex(usRegex);
            Regex caPostalRegex = new Regex(caRegex);
            Match usMatch = usZipRegex.Match(tb.Text);
            Match caMatch = caPostalRegex.Match(tb.Text);
            if (usMatch.Success || caMatch.Success) return true;
            else {
                MessageBox.Show(message);
                return false;
            }
        }

        // Don - overloaded method to check masked text boxes for presence
        public static bool IsPresent(MaskedTextBox mtb) {
            if (mtb.Text != "") 
                return true;
            else return false;
        }
    }//Class Validator
}
