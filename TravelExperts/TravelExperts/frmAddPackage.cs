﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelExperts {
    public partial class frmAddPackage : Form {
        Package package;
        public frmAddPackage() {
            InitializeComponent();
        }

        private void btnClear_Click(object sender, EventArgs e) {
            txtName.Text = "";
            txtDesc.Text = "";
            dtpStartDate.Text = "";
            dtpEndDate.Text = "";
            txtBasePrice.Text = "";
            txtAgencyCommission.Text = "";
        }

        private void btnSavePackage_Click(object sender, EventArgs e) {
            if (txtPackageId.Text == "") {
                package = new Package(0, txtName.Text, Convert.ToDateTime(dtpStartDate.Text),
                          Convert.ToDateTime(dtpEndDate.Text), txtDesc.Text, Convert.ToDecimal(txtBasePrice.Text), 
                          Convert.ToDecimal(txtAgencyCommission.Text));
                PackagesDB.AddPackage(package);
            }
        }

        private void btnExit_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void frmAddPackage_Load(object sender, EventArgs e) {
            List<Products_Suppliers> prodSupList = new List<Products_Suppliers>();
            prodSupList = Products_SuppliersDB.GetAllProductsSuppliers();



        }

        private void populateListBy() {
            //populate dropdown using ProductDB.GetAll();
            // populate listview using SupplierDB.Get

            // cmboProdSup.Items.Add

        }

        //private void buildTree(List<Products_Suppliers> psList, string arrangeBy = "P") {

            //string pNode = "Product: ";
            //string sNode = "Supplier: ";

            //if (arrangeBy == "P") { // arrange list by Products
            //    foreach (var p in psList) {
            //        pNode += p.ProductID.ToString();
            //        sNode += p.SupplierID.ToString();

            //        if (!treeProductsSuppliers.Nodes.ContainsKey(pNode)) {
            //            //MessageBox.Show("I'm in");
            //            TreeNode productNode = new TreeNode(pNode);
            //            productNode.Name = pNode;
            //            treeProductsSuppliers.Nodes.Add(productNode);
                        
            //        }
            //        treeProductsSuppliers.Nodes[pNode].Nodes.Add(sNode);
            //        pNode = "Product: ";
            //        sNode = "Supplier: ";
            //    }

            //} else { // arrange list by Suppliers
            //    // loop through Products_Suppliers list
            //        // main node gets Suppliers
            //        // child nodes get Products
            //}

            //node = treeProductsSuppliers.Nodes.Add("Master node");
            //node.Nodes.Add("Child node");
            //node.Nodes.Add("Child node 2");

            //node = treeProductsSuppliers.Nodes.Add("Master node 2");
            //node.Nodes.Add("mychild");
            //node.Nodes.Add("mychild");
        //}
        //for (int i = 0; i < strArrGroups.Length; i++) {
        //    //int groupIndex = lsvProductsSuppliers.Groups.Add(new ListViewGroup(strArrGroups[i], HorizontalAlignment.Left));
        //    //for (int j = 0; j < strArrItems.Length; j++) {
        //    //    ListViewItem lvi = new ListViewItem(strArrItems[j]);
        //    //    lvi.SubItems.Add("Hasta la Vista, Mon Cherri!");
        //    //    lsvProductsSuppliers.Items.Add(lvi);
        //    //    lsvProductsSuppliers.Groups[i].Items.Add(lvi);

        //    //}
        //}

        //ListViewGroup[] groupArr = new ListViewGroup[Products_SuppliersDB.CountProducts()];
        ////ListViewItem[] itemArr = new ListViewItem[Products_SuppliersDB.CountAll()];

        //// build string list for products
        //foreach (var p in prodSupList) { // make the listview group headers
        //    int index = 0;
        //    ListViewGroup newGroup = new ListViewGroup(p.ProductID.ToString());
        //    ListViewItem newItem = new ListViewItem(p.SupplierID.ToString());
        //    if (!groupArr.Contains(newGroup)) { // header doesn't exist, so add it
        //        lsvProductsSuppliers.Groups.Add(newGroup);
        //    }
        //else { // item is already in the list, add item to header group

        //  }

        //    index++;

        //}

        //lsvProductsSuppliers.Groups.AddRange(groupArr);
        //lsvProductsSuppliers.Items.Add();
        }
     }
