﻿namespace TravelExperts {
    partial class frmAddProduct {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.cmboProduct = new System.Windows.Forms.ComboBox();
            this.dgvProdSup = new System.Windows.Forms.DataGridView();
            this.ProductId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SupplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SupplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNewProduct = new System.Windows.Forms.TextBox();
            this.lblNewProd = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.cmboSupplier = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkNewProd = new System.Windows.Forms.CheckBox();
            this.txtNewSupplier = new System.Windows.Forms.TextBox();
            this.chkNewSup = new System.Windows.Forms.CheckBox();
            this.lblNewSup = new System.Windows.Forms.Label();
            this.productsDBBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProdSup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsDBBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // cmboProduct
            // 
            this.cmboProduct.FormattingEnabled = true;
            this.cmboProduct.Location = new System.Drawing.Point(49, 75);
            this.cmboProduct.Name = "cmboProduct";
            this.cmboProduct.Size = new System.Drawing.Size(259, 23);
            this.cmboProduct.TabIndex = 0;
            this.cmboProduct.SelectedIndexChanged += new System.EventHandler(this.cmboProduct_SelectedIndexChanged);
            // 
            // dgvProdSup
            // 
            this.dgvProdSup.AllowUserToAddRows = false;
            this.dgvProdSup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProdSup.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductId,
            this.SupplierName,
            this.SupplierId,
            this.Supplier});
            this.dgvProdSup.Location = new System.Drawing.Point(49, 223);
            this.dgvProdSup.MultiSelect = false;
            this.dgvProdSup.Name = "dgvProdSup";
            this.dgvProdSup.Size = new System.Drawing.Size(540, 244);
            this.dgvProdSup.TabIndex = 1;
            // 
            // ProductId
            // 
            this.ProductId.HeaderText = "Prod ID";
            this.ProductId.Name = "ProductId";
            this.ProductId.Width = 40;
            // 
            // SupplierName
            // 
            this.SupplierName.DataPropertyName = "ProductName";
            this.SupplierName.HeaderText = "Product";
            this.SupplierName.Name = "SupplierName";
            this.SupplierName.Width = 150;
            // 
            // SupplierId
            // 
            this.SupplierId.HeaderText = "Sup ID";
            this.SupplierId.Name = "SupplierId";
            this.SupplierId.Width = 40;
            // 
            // Supplier
            // 
            this.Supplier.HeaderText = "Supplier";
            this.Supplier.Name = "Supplier";
            this.Supplier.Width = 250;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Choose a Product";
            // 
            // txtNewProduct
            // 
            this.txtNewProduct.Location = new System.Drawing.Point(49, 155);
            this.txtNewProduct.Name = "txtNewProduct";
            this.txtNewProduct.Size = new System.Drawing.Size(256, 23);
            this.txtNewProduct.TabIndex = 3;
            this.txtNewProduct.Visible = false;
            // 
            // lblNewProd
            // 
            this.lblNewProd.AutoSize = true;
            this.lblNewProd.Location = new System.Drawing.Point(49, 132);
            this.lblNewProd.Name = "lblNewProd";
            this.lblNewProd.Size = new System.Drawing.Size(141, 15);
            this.lblNewProd.TabIndex = 4;
            this.lblNewProd.Text = "Enter New Product Name";
            this.lblNewProd.Visible = false;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(50, 489);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(212, 54);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "Save";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(393, 489);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(196, 54);
            this.btnDelete.TabIndex = 6;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(514, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 40);
            this.btnExit.TabIndex = 7;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // cmboSupplier
            // 
            this.cmboSupplier.FormattingEnabled = true;
            this.cmboSupplier.Location = new System.Drawing.Point(333, 75);
            this.cmboSupplier.Name = "cmboSupplier";
            this.cmboSupplier.Size = new System.Drawing.Size(256, 23);
            this.cmboSupplier.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(335, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 15);
            this.label2.TabIndex = 11;
            this.label2.Text = "Choose <Supplier> to add";
            // 
            // chkNewProd
            // 
            this.chkNewProd.AutoSize = true;
            this.chkNewProd.Location = new System.Drawing.Point(52, 106);
            this.chkNewProd.Name = "chkNewProd";
            this.chkNewProd.Size = new System.Drawing.Size(133, 19);
            this.chkNewProd.TabIndex = 9;
            this.chkNewProd.Text = "Create New Product";
            this.chkNewProd.UseVisualStyleBackColor = true;
            this.chkNewProd.CheckedChanged += new System.EventHandler(this.chkNewProd_CheckedChanged);
            // 
            // txtNewSupplier
            // 
            this.txtNewSupplier.Location = new System.Drawing.Point(333, 155);
            this.txtNewSupplier.Name = "txtNewSupplier";
            this.txtNewSupplier.Size = new System.Drawing.Size(256, 23);
            this.txtNewSupplier.TabIndex = 12;
            this.txtNewSupplier.Visible = false;
            // 
            // chkNewSup
            // 
            this.chkNewSup.AutoSize = true;
            this.chkNewSup.Location = new System.Drawing.Point(345, 106);
            this.chkNewSup.Name = "chkNewSup";
            this.chkNewSup.Size = new System.Drawing.Size(136, 19);
            this.chkNewSup.TabIndex = 13;
            this.chkNewSup.Text = "Create New Supplier";
            this.chkNewSup.UseVisualStyleBackColor = true;
            this.chkNewSup.CheckedChanged += new System.EventHandler(this.chkNewSup_CheckedChanged);
            // 
            // lblNewSup
            // 
            this.lblNewSup.AutoSize = true;
            this.lblNewSup.Location = new System.Drawing.Point(330, 132);
            this.lblNewSup.Name = "lblNewSup";
            this.lblNewSup.Size = new System.Drawing.Size(144, 15);
            this.lblNewSup.TabIndex = 14;
            this.lblNewSup.Text = "Enter New Supplier Name";
            this.lblNewSup.Visible = false;
            // 
            // productsDBBindingSource
            // 
            this.productsDBBindingSource.DataSource = typeof(TravelExperts.ProductsDB);
            // 
            // frmAddProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(650, 562);
            this.Controls.Add(this.lblNewSup);
            this.Controls.Add(this.chkNewSup);
            this.Controls.Add(this.txtNewSupplier);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmboSupplier);
            this.Controls.Add(this.chkNewProd);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lblNewProd);
            this.Controls.Add(this.txtNewProduct);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvProdSup);
            this.Controls.Add(this.cmboProduct);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmAddProduct";
            this.Text = "Product & Supplier Maintenance";
            this.Load += new System.EventHandler(this.frmAddProduct_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProdSup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsDBBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmboProduct;
        private System.Windows.Forms.DataGridView dgvProdSup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNewProduct;
        private System.Windows.Forms.Label lblNewProd;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.BindingSource productsDBBindingSource;
        private System.Windows.Forms.ComboBox cmboSupplier;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier;
        private System.Windows.Forms.CheckBox chkNewProd;
        private System.Windows.Forms.TextBox txtNewSupplier;
        private System.Windows.Forms.CheckBox chkNewSup;
        private System.Windows.Forms.Label lblNewSup;
    }
}