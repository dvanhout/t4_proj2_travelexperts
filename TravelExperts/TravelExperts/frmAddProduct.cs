﻿/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  Product and supplier maintenance form
 *              NOTE:  title of form is not as descriptive as
 *              it could be as it morphed over the course of the
 *              project and I didn't want to alter it substantially
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelExperts {
    public partial class frmAddProduct : Form {
        public frmAddProduct() {
            InitializeComponent();
        }

        // data grid view table
        DataTable supplierTable = new DataTable();
        // list of products_suppliers for datagrid
        List<Products_Suppliers> prodSupList = new List<Products_Suppliers>();
        // list of all products for combo box
        List<Product> prodList = new List<Product>();
        // list of all suppliers for combo box
        List<Supplier> supList = new List<Supplier>();


        // initialize combo boxes and data grid
        private void frmAddProduct_Load(object sender, EventArgs e) {
            loadComboProducts(); // load combo boxes
            loadComboSuppliers();
            loadDataGrid();
            supplierTable.Columns.Add("Supplier Id", typeof(int));
            supplierTable.Columns.Add("Supplier", typeof(string));
        }


        // refreshes data grid items
        private void loadDataGrid() {
            // load data grid 
            dgvProdSup.Rows.Clear();
            prodSupList = Products_SuppliersDB.GetProdSupByProdName(cmboProduct.Text);
            foreach(Products_Suppliers ps in prodSupList) {
                Product prod = ProductsDB.GetProduct(ps.ProductID);
                Supplier sup = SuppliersDB.GetSupplier(ps.SupplierID);

                dgvProdSup.Rows.Add(prod.ProductID, prod.ProdName, sup.SupplierID, sup.SupName);
            }
        }


        // refreshes products combo box
        private void loadComboProducts() {
            cmboProduct.Items.Clear();
            prodList = ProductsDB.GetAllProducts();
            foreach (Product p in prodList) {
                cmboProduct.Items.Add(p.ProdName.ToString());
            }
            cmboProduct.SelectedItem = 0; // set selected item to first in index          
        }


        // refreshes suppliers combo box
        private void loadComboSuppliers() {
            cmboSupplier.Items.Clear();
            supList = SuppliersDB.GetAllSuppliers();
            foreach (Supplier s in supList) {
                cmboSupplier.Items.Add(s.SupName.ToString());
            }
            cmboSupplier.SelectedItem = 0; // set selected item to first in index
        }


        // exit button event
        private void btnExit_Click(object sender, EventArgs e) {
            this.Close();
        }


        // delete button event, capable of removing multiple items
        private void btnDelete_Click(object sender, EventArgs e) {
            foreach (DataGridViewRow row in dgvProdSup.Rows) {
                if (row.Selected) {
                    Products_Suppliers ps = new Products_Suppliers();
                    ps = Products_SuppliersDB.GetProductSupplierByName(
                            row.Cells[3].Value.ToString(), row.Cells[1].Value.ToString());
                    if(Products_SuppliersDB.DeleteProdSup(ps.ProductSupplierID)) {
                        MessageBox.Show("Supplier was deleted");
                    } else {
                        MessageBox.Show("Something went wrong, supplier was not deleted");
                    }
                }
            }
            loadDataGrid(); // reload the grid
        }


        // save button event
        private void btnAdd_Click(object sender, EventArgs e) {
            // enter new product
            if (chkNewProd.Checked) {
                if (Validator.isEmpty(txtNewProduct)) { // validate entry
                    MessageBox.Show("Product must have a name");
                } else if (ProductsDB.AddProduct(txtNewProduct.Text)) { // add the product
                    MessageBox.Show(txtNewProduct.Text + " was added successfully");
                } else MessageBox.Show("There was a problem adding the product");

                //enter new supplier
                } else if (chkNewSup.Checked) {
                if (Validator.isEmpty(txtNewSupplier)) { // validate entry
                    MessageBox.Show("Supplier must have a name");
                } else if (SuppliersDB.AddSupplier(txtNewSupplier.Text.ToUpper())) { // add the supplier
                    MessageBox.Show(txtNewSupplier.Text + " was added successfully");
                } else MessageBox.Show("There was a problem adding the supplier");

            // create product/supplier entry in database
            } else if(cmboProduct.Text != "" && cmboSupplier.Text != "") {
                int psId = Products_SuppliersDB.AddProductSupplierByName(cmboProduct.Text, cmboSupplier.Text);
                if (psId < 1) { // record entry failure
                    MessageBox.Show("There was something wrong with the insert");
                }
            } else { // failed validation
                MessageBox.Show("Product and Supplier must not be empty");
            }
            // refresh views
            loadComboProducts();
            loadComboSuppliers();
            loadDataGrid();
        }


        // refresh data grid on combo box change
        private void cmboProduct_SelectedIndexChanged(object sender, EventArgs e) {      
            loadDataGrid();
        }


        // enable/disable form fields/controls based on check boxes
        private void chkNewSup_CheckedChanged(object sender, EventArgs e) {
            if (chkNewSup.Checked) {
                // enable entry of new product
                lblNewSup.Visible = true;
                txtNewSupplier.Visible = true;

                // disable entry of new product
                txtNewProduct.Visible = false;
                lblNewProd.Visible = false;

                // disable new product check box
                chkNewProd.Checked = false;
                chkNewProd.Enabled = false;
                
                // disable product/supplier combo boxes
                cmboProduct.Enabled = false;
                cmboSupplier.Enabled = false;

            } else { // chkNewProd is not checked
                lblNewSup.Visible = false;
                txtNewSupplier.Visible = false;

                // enable check box for product
                chkNewProd.Checked = false;
                chkNewProd.Visible = true;
                chkNewProd.Enabled = true;
                
                // enable product/supplier combo boxes
                cmboProduct.Enabled = true;
                cmboSupplier.Enabled = true;
            }
        }

        // new product check box checked event
        private void chkNewProd_CheckedChanged(object sender, EventArgs e) {
            if (chkNewProd.Checked) {
                // enable entry of new product
                lblNewProd.Visible = true;
                txtNewProduct.Visible = true;

                // disable entry of new product
                txtNewSupplier.Visible = false;
                lblNewSup.Visible = false;

                // disable new product check box
                chkNewSup.Checked = false;
                chkNewSup.Enabled = false;

                // disable supplier drop down
                cmboSupplier.Enabled = false;
                cmboProduct.Enabled = false;

            } else { // chkNewProd is not checked
                lblNewProd.Visible = false;
                txtNewProduct.Visible = false;

                // enable check box for product
                chkNewSup.Checked = false;
                chkNewSup.Visible = true;
                chkNewSup.Enabled = true;

                cmboSupplier.Enabled = true;
                cmboProduct.Enabled = true;
            }
        }
    }
}
