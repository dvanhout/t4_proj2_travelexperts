﻿/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  Form to maintain Supplier Contacts
 *              This is outside the scope of the assignment
 *            
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static TravelExperts.frmMain;

namespace TravelExperts {
    public partial class frmAddSupplier : Form, IChildFormInterface {

        // load data
        List<Supplier> suppliers = new List<Supplier>();
        List<Product> products = new List<Product>();
        List<Products_Suppliers> prodSupList = new List<Products_Suppliers>();

        int supplierContactId;
        int selectedCompanyId;

        SupplierContact activeSupCon = new SupplierContact();

        List<Products_Suppliers> removePSList = new List<Products_Suppliers>();

        // gets data from open supplier contact form, or none if spawning a new form
        public frmAddSupplier(int supContId = 0, int supplierId = 0) {
            InitializeComponent();
            supplierContactId = supContId;
            selectedCompanyId = supplierId;
        }


        // clear all fields
        private void btnClear_Click(object sender, EventArgs e) {
            clearFields();
        }

        private void clearFields() {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtAddress.Text = "";
            txtCity.Text = "";
            txtProv.Text = "";
            txtPostal.Text = "";
            txtCountry.Text = "";
            txtBusPhone.Text = "";
            cmboCompany.Text = "";
            txtEmail.Text = "";
            txtURL.Text = "";
            txtSupConId.Text = "";
            txtFax.Text = "";
            lvProducts.Items.Clear();
            btnSave.Text = "Save";
        }


        // adds products associated with the supplier
        private void addProductToList(string supName, string prodName) {
            // make the new listviewitem
            string[] arr = new string[2];
            arr[0] = supName;
            arr[1] = prodName;
            ListViewItem lvi = new ListViewItem(arr);
            lvi.Name = arr[0]; // lvi needs a name for comparison
            lvProducts.Items.Add(lvi);          
          }


        // load event, determines if form is for a new entry or modifying an existing record
        private void frmAddSupplier_Load(object sender, EventArgs e) {

            if (supplierContactId == 0) { // this is a new entry
                txtSupConId.Text = "";
            } else { // this is an existing entry, so update
                txtSupConId.Text = supplierContactId.ToString();
            }

            suppliers = SuppliersDB.GetAllSuppliers();
            products = ProductsDB.GetAllProducts();
            prodSupList = Products_SuppliersDB.GetAllProductsSuppliers();


            // populate suppliers combo box         
            cmboCompany.DisplayMember = "SupName";
            cmboCompany.ValueMember = "SupplierId";
            cmboCompany.DataSource = suppliers;

            if (selectedCompanyId != 0) { // set text of combo if coming from frmOpenSuppliers form
                Supplier s = SuppliersDB.GetSupplier(selectedCompanyId);
                cmboCompany.Text = s.SupName;
                displayList();
            }

            if (txtSupConId.Text == "") btnSave.Text = "Save";
                else btnSave.Text = "Update";
        }


        // list of products for the supplier
        private void displayList() {
            lvProducts.Items.Clear();
            // get list of product suppliers by id for the supplier
            List<Products_Suppliers> psList = new List<Products_Suppliers>();
            psList = Products_SuppliersDB.GetProductsSuppliersBySupID(selectedCompanyId);

            foreach (Products_Suppliers ps in psList) {
                Product p = new Product();
                p = ProductsDB.GetProduct(ps.ProductID);
                string[] arr = new string[2];
                arr[0] = p.ProductID.ToString();
                arr[1] = p.ProdName;
                ListViewItem lsv = new ListViewItem(arr);
                lvProducts.Items.Add(lsv);
            }
        }


        // open new form to search for a company
        private void btnFindCompany_Click(object sender, EventArgs e) {
            frmFindSupplier findSuppliers = new frmFindSupplier(suppliers);
            if (findSuppliers.ShowDialog() == DialogResult.OK) {
                cmboCompany.Text = findSuppliers.selectedSupName;
                selectedCompanyId = findSuppliers.selectedSupId;
            }
        }


        // button click save event
        private void btnSave_Click(object sender, EventArgs e) {
            SaveData();
        }

        
        // button click delete event
        private void btnDeleteContact_Click(object sender, EventArgs e) {
            RemoveData();
        }


        // if supplier contact is changed
        private void txtSupConId_TextChanged(object sender, EventArgs e) {
            SupplierContact sc = null;
            Product p = null;
            if (txtSupConId.Text == "") // it's a new record
                btnDeleteContact.Enabled = false;
            else { // it's an update
                sc = new SupplierContact();
                sc = SupplierContactsDB.GetSupCon(Convert.ToInt32(txtSupConId.Text));
                txtFirstName.Text = sc.SupConFirstName;
                txtLastName.Text = sc.SupConLastName ?? "";
                txtAddress.Text = sc.SupConAddress ?? "";
                txtCity.Text = sc.SupConCity ?? "";
                txtProv.Text = sc.SupConProv ?? "";
                txtPostal.Text = sc.SupConPostal ?? "";
                txtCountry.Text = sc.SupConCountry ?? "";
                txtBusPhone.Text = sc.SupConBusPhone ?? "";
                txtEmail.Text = sc.SupConEmail ?? "";
                txtURL.Text = sc.SupConURL ?? "";
                txtFax.Text = sc.SupConFax ?? "";

                activeSupCon = sc;

                btnDeleteContact.Enabled = true; // enable delete button
            }
        }


        // supplier company drop down item changed event
        private void cmboCompany_SelectedIndexChanged(object sender, EventArgs e) {
            selectedCompanyId = Convert.ToInt32(cmboCompany.SelectedValue);
            displayList();
        }


        // save form data, but validate
        public void SaveData() {
            bool checksPassed = true;
            // get items from text boxes and validate
            if (Validator.IsPresent(txtFirstName, "First Name is required")
                && Validator.IsPresent(txtLastName, "Last Name is required")
                && Validator.IsPresent(txtAddress, "Address is Required")
                && Validator.IsPresent(txtCity, "City is required")
                && Validator.Email(txtEmail, "Enter a valid email address")
                && Validator.Phone(txtBusPhone, "Enter a valid phone number")) {

                // assign values and validate as you do
                SupplierContact supContact = new SupplierContact();
                supContact.SupConFirstName = txtFirstName.Text;
                supContact.SupConLastName = txtLastName.Text;
                supContact.SupConAddress = txtAddress.Text;
                supContact.SupConCity = txtCity.Text;
                supContact.SupConProv = txtProv.Text;
                if (Validator.IsPresent(txtPostal) && Validator.Postal(txtPostal, "Postal is an invalid format")) {
                    supContact.SupConPostal = txtPostal.Text;
                } else {
                    checksPassed = false;
                }
                supContact.SupConCountry = txtCountry.Text;
                supContact.SupConBusPhone = txtBusPhone.Text;
                if (Validator.IsPresent(txtFax) && Validator.Phone(txtFax, "Fax is an invalid format")) {
                    supContact.SupConFax = txtFax.Text;
                } else {
                    checksPassed = false;
                }
                supContact.SupConEmail = txtEmail.Text;
                if (Validator.IsPresent(txtURL) && Validator.URL(txtURL, "Web address is an invalid format")) {
                    supContact.SupConURL = txtURL.Text;
                } else {
                    checksPassed = false;
                }
                supContact.SupConCompany = cmboCompany.Text;
                supContact.SupplierId = Convert.ToInt32(cmboCompany.SelectedValue);

                if (checksPassed) {
                    // if id is present
                    if (txtSupConId.Text == "") {
                        // insert record
                        supContact.SupplierContactId = 0; // give a zero value if no id
                        int id = SupplierContactsDB.AddSupCon(supContact); // do the insert query
                        if (id != 0) { // if successful, display result message
                            MessageBox.Show(supContact.SupConFirstName + " " + supContact.SupConLastName + " was added with id: " + id);
                            txtSupConId.Text = id.ToString();
                            btnSave.Text = "Update";
                        } else { // insert query is unsuccessful
                            MessageBox.Show("There was a problem adding the customer");
                        }

                    } else { // Contact already exists so update query
                        supContact.SupplierContactId = Convert.ToInt32(txtSupConId.Text); // assign the id
                        if (SupplierContactsDB.UpdateSupCon(supContact.SupplierContactId, supContact)) {
                            // remove product suppliers from list
                            foreach (Products_Suppliers ps in removePSList) {
                                if (ps != null) {
                                    Products_SuppliersDB.DeleteProdSup(ps.ProductSupplierID);
                                }
                            }
                            // clear the items to be removed list
                            removePSList.Clear();

                            MessageBox.Show(supContact.SupConFirstName + " " + supContact.SupConLastName + " was successfully updated");
                        } else {
                            MessageBox.Show("The update was unsuccessful");
                        }
                    }
                } else MessageBox.Show("The save or update was unsuccessful");
            }
        }


        // delete data from database
        public void RemoveData() {
            if (txtSupConId.Text != "") {
                int scid = Convert.ToInt32(txtSupConId.Text);
                clearFields();
                if (SupplierContactsDB.DeleteSupCon(scid)) {
                    MessageBox.Show("Supplier Contact has been removed from the database");
                } else {
                    MessageBox.Show("There was a problem removing the Supplier Contact from the database");
                }
            }
        }


        // open new search form to find a supplier contact
        private void btnFindSupCon_Click(object sender, EventArgs e) {
            frmOpenSupplier supplierForm = new frmOpenSupplier();
            if (supplierForm.ShowDialog() == DialogResult.OK) {
                supplierContactId = supplierForm.selectedSupplierContactId;
                selectedCompanyId = supplierForm.selectedSupplierId;
                frmAddSupplier_Load(sender, e);
            }
        }
    }
}
