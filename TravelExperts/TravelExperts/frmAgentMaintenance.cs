﻿/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  Allows user to alter password 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelExperts {
    public partial class frmAgentMaintenance : Form {
        Agent agent;
        public frmAgentMaintenance(Agent agt) {
            InitializeComponent();
            agent = agt; // get currently logged in agent
        }

        private void btnUpdatePwd_Click(object sender, EventArgs e) {
            string oldPwd = agent.Password; // store old pwd in case something goes wrong
            try {
 
                // check that text boxes are filled out               
                if (Validator.IsPresent(txtOldPwd, "Please enter current password") &&
                    Validator.IsPresent(txtNewPwd, "No password entered") &&
                    Validator.IsPresent(txtConfirmPwd, "please re-enter password")) {
                    
                    // check that old pwd matches db and new pwd/confirm pwd match
                    if (AgentDB.LoginOk(agent.Username, txtOldPwd.Text) &&
                        txtNewPwd.Text == txtConfirmPwd.Text) {
                        // passed checks, confirmation message box
                        string msg = "You are about to change your password, are you sure?";
                        string cap = "Warning: Password change";
                        MessageBoxButtons btns = MessageBoxButtons.OKCancel;
                        DialogResult result;
                        result = MessageBox.Show(msg, cap, btns);

                        // user confirmed
                        if (result == System.Windows.Forms.DialogResult.OK) {
                            //update logged in agent at form level
                            agent.Password = txtNewPwd.Text;
                            // update at DB level
                            AgentDB.UpdateAgent(agent.AgentId, agent);

                            // user cancelled
                        } else if (result == System.Windows.Forms.DialogResult.Cancel) {
                            // revert logged in agent to old password
                            agent.Password = oldPwd;
                        }
                        this.Close();
                    } else {
                        MessageBox.Show("There was a problem with your passwords, please try again");
                    }
                }                                   
            } catch (Exception ex) {
                MessageBox.Show("There was a problem with the password update: " + ex.Message);
                // there was a problem, revert logged in agent to old password
                agent.Password = oldPwd;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
