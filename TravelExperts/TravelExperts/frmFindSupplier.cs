﻿/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  Helper form to allow user to search for supplier contacts
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelExperts {
    public partial class frmFindSupplier : Form {

        // shared data
        public int selectedSupId { get; set; }
        public string selectedSupName { get; set; }

        List<Supplier> supList = new List<Supplier>();

        // initialize the list 
        public frmFindSupplier(List<Supplier> suppliers) {
            supList = suppliers;
            InitializeComponent();
        }


        // load event
        private void frmFindSupplier_Load(object sender, EventArgs e) {
            displayList(supList);
        }

        // refreshes the list
        private void displayList(List<Supplier> supList) {
            lvCompany.Items.Clear();
            foreach (var s in supList) {
                string[] arr = new string[2];
                arr[0] = s.SupplierID.ToString();
                arr[1] = s.SupName;
                lvCompany.Items.Add(new ListViewItem(arr));
            }
        }

        // choose selected item to return data to previous form
        private void btnSelect_Click(object sender, EventArgs e) {
            if (this.lvCompany.SelectedItems.Count > 0) {
                // store values get values
                selectedSupId = Convert.ToInt32(lvCompany.SelectedItems[0].SubItems[0].Text); 
                selectedSupName = lvCompany.SelectedItems[0].SubItems[1].Text;
            }
            this.Close(); // close the form
        }


        // enable select button when user chooses a record
        private void lvCompany_SelectedIndexChanged(object sender, EventArgs e) {
            if (lvCompany.SelectedItems.Count > 0) { 
                btnSelect.Enabled = true;
            } else {
                btnSelect.Enabled = false;
            }
        }


        // leave the form with no data
        private void btnCancel_Click(object sender, EventArgs e) {
            this.Close();
        }


        // responsive filter, updates list on every character entered
        private void txtFilter_TextChanged(object sender, EventArgs e) {
            List<Supplier> filteredSupList = null;
            if (txtFilter.Text != "") {
                filteredSupList = new List<Supplier>();
                filteredSupList = (from s in supList
                                   where s.SupName.Contains(txtFilter.Text.ToUpper())
                                   select s).ToList();
                displayList(filteredSupList);
            } else {
                displayList(supList);
            }
        }
    }
}
