﻿/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  login form 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelExperts {
    public partial class frmLogin : Form {

        // data for frmMain upon user login
        public string Usr { get; set; } 
        public string Pwd { get; set; }

        public frmLogin() {
            InitializeComponent();
        }


        // exit the program
        private void btnCancel_Click(object sender, EventArgs e) {
            //Application.Exit();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        // clear username and password text boxes
        private void btnClear_Click(object sender, EventArgs e) {
            txtUsername.Text = "";
            txtPassword.Text = "";
        }

        // try to login user
        private void btnLogin_Click(object sender, EventArgs e) {
            if (Validator.IsPresent(txtUsername, "Username") &&
                Validator.IsPresent(txtPassword, "Password") &&
                LoginAgent()) {
                this.DialogResult = DialogResult.OK; // signal to frmMain
                this.Close(); // close the login form
            }
        }


        // test to see if the username and password return value
        private bool LoginAgent() {
            if (AgentDB.LoginOk(txtUsername.Text, txtPassword.Text)) {
                // assign inter-form data
                Usr = txtUsername.Text;
                Pwd = txtPassword.Text;
                return true;
            } else {
                MessageBox.Show("There is a problem with your login, please try again");
                return false;
            }
        }      
    }
}
