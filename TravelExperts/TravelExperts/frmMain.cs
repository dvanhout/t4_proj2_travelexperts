﻿/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  Main parent form for all others, using Mdi format. 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelExperts {
    public partial class frmMain : Form {

        Agent agent; /* = new Agent();*/ // store currently logged in agent
        List<Form> openForms = new List<Form>(); // get a list of all 

        // lets parent mdi form call child form save function
        public interface IChildFormInterface {
            void SaveData();
            void RemoveData();
        }
    

        // thread used by splash screen progress bar
        public frmMain() {
            Thread t = new Thread(new ThreadStart(SplashScrn));
            Thread.Sleep(5000);
            InitializeComponent();
            t.Abort();
        }


        // open new SpashForm
        public void SplashScrn() {
            Application.Run(new frmSplash());
        }


        // on load go to login page
        private void frmMain_Load(object sender, EventArgs e) {
            goToLogin();
        }


        // open login form to verify agent ... disable current user
        private void goToLogin() {
            frmLogin loginForm = new frmLogin();
            if (loginForm.ShowDialog() == DialogResult.OK) {
                agent = new Agent();
                agent = AgentDB.GetAgent(loginForm.Usr, loginForm.Pwd);
                lblActiveUser.Text = "Current User: " + agent.AgtFirstName + " " + agent.AgtLastName;          
            }
            enableDisableMnu();
        }


        // checks to see if an agent has been set (is logged in) and enables/disables controls
        private void enableDisableMnu() {
            if (agent == null) { 
                mnuFileNew.Enabled = false;
                mnuFileOpen.Enabled = false;
                mnuFileClose.Enabled = false;
                mnuWindow.Enabled = false;
                mnuAgent.Enabled = false;
                mnuFileLogoff.Text = "Login";
            } else {
                mnuFileNew.Enabled = true;
                mnuFileOpen.Enabled = true;
                mnuFileClose.Enabled = true;
                mnuWindow.Enabled = true;
                mnuAgent.Enabled = true;
                mnuFileLogoff.Text = "Logoff";
            }
        }


        // ****************************menu item events**************************

        // menu exit
        private void mnuFileExit_Click(object sender, EventArgs e) {
            Application.Exit();
        }


        // if user logs off trigger warning to save all open files
        private void mnuFileLogoff_Click(object sender, EventArgs e) {
            if (MdiChildren.Any()) { //there are open forms
                string msg = "Unsaved work will be lost, do you want to save now?";
                string cap = "Warning: Unsaved work";
                // warn the user
                MessageBoxButtons btns = MessageBoxButtons.YesNo;
                DialogResult result;
                result = MessageBox.Show(msg, cap, btns);

                // user wants to save
                if (result == System.Windows.Forms.DialogResult.Yes) {

                // user wants to exit so close all open forms
                } else if (result == System.Windows.Forms.DialogResult.No) {
                    foreach (Form frm in this.MdiChildren) {
                        frm.Close();
                        logoffUser();
                    }
                } 
            } else {
                logoffUser();
            }
        }

        // helper 
        private void logoffUser() {
            lblActiveUser.Text = "Logged Off";
            agent = null;
            goToLogin();
        }


        // display open child forms in cascade format
        private void mnuWindowCascade_Click(object sender, EventArgs e) {
            this.LayoutMdi(MdiLayout.Cascade);
        }


        // tile open child forms horizontally
        private void mnuWindowTileHorizontal_Click(object sender, EventArgs e) {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }


        // tile open forms vertically
        private void mnuWindowTileVertical_Click(object sender, EventArgs e) {
            this.LayoutMdi(MdiLayout.TileVertical);
        }


        private void mnuFileNewPackage_Click(object sender, EventArgs e) {
            // create new form to add a package as mdi child
            Form addPackageForm = new Mussie.Forms.frmAddPackage();
            addPackageForm.MdiParent = this;
            addPackageForm.Show();
            openForms.Add(addPackageForm);
        }


        private void mnuFileNewSupplier_Click(object sender, EventArgs e) {
            Form addSupplierForm = new frmAddSupplier();
            addSupplierForm.MdiParent = this;
            addSupplierForm.Show();
        }


        private void mnuFileOpenPackage_Click(object sender, EventArgs e) {
            Form openPackageForm = new Mussie.Forms.frmUpdatePackage();
            openPackageForm.MdiParent = this;
            openPackageForm.Show();
            openForms.Add(openPackageForm);
        }


        private void mnuFileOpenProduct_Click(object sender, EventArgs e) {
            Form addProductForm = new frmAddProduct();
            addProductForm.MdiParent = this;
            addProductForm.Show();
        }


        private void mnuFileOpenSupplier_Click(object sender, EventArgs e) {
            Form openSupplierForm = new frmOpenSupplier();
            openSupplierForm.MdiParent = this;
            openSupplierForm.Show();
        }


        // closes the active form, removes it from openforms list
        private void mnuFileClose_Click(object sender, EventArgs e) {
            Form activeForm = this.ActiveMdiChild;
            if (activeForm != null) { 
                activeForm.Close();
                // check to see if its on the unsaved forms list?  
                openForms.Remove(activeForm); // take it off the open forms lift
            }
        }


        private void mnuAgentChangePwd_Click(object sender, EventArgs e) {
            Form agentMaintenance = new frmAgentMaintenance(agent);
            agentMaintenance.ShowDialog();
        }
    }
}