﻿/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  Form to search supplier contacts and send data
 *              to supplier contact maintenance
 *            
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelExperts {
    public partial class frmOpenSupplier : Form {

        public int selectedSupplierContactId { get; set; }
        public int selectedSupplierId { get; set; }

        List<SupplierContact> supConList = new List<SupplierContact>();
        List<Supplier> supList = new List<Supplier>();

        bool sendToPreviousForm;

        // boolean flags whether to send data to an already opened form
        // depending on how user accesses the form (menu or find button)
        public frmOpenSupplier(bool toOpenedForm = false) {
            InitializeComponent();
            sendToPreviousForm = toOpenedForm;
        }


        // select item button click event - send to opened form or create new form?
        private void btnSelect_Click(object sender, EventArgs e) {
            selectedSupplierId = (from s in supList
                                  where s.SupName == lvSupplierContacts.SelectedItems[0].SubItems[0].Text
                                  select s.SupplierID).FirstOrDefault();
            selectedSupplierContactId = Convert.ToInt32(lvSupplierContacts.SelectedItems[0].SubItems[1].Text);

            // load data into previous form
            if (sendToPreviousForm && Application.OpenForms.OfType<frmAddSupplier>().Count() == 1) {                
                this.DialogResult = DialogResult.OK; // signal to previous form
                this.Close(); // close the form
            
            // create a new form
            } else { 
                frmAddSupplier addSupplierForm = new frmAddSupplier(selectedSupplierContactId, selectedSupplierId);
                addSupplierForm.MdiParent = this.ParentForm;
                addSupplierForm.Show();
                this.Close();
            }
        }

        
        // load event - display items using helper
        private void frmOpenSupplier_Load(object sender, EventArgs e) {
            supList = SuppliersDB.GetAllSuppliers();
            supConList = SupplierContactsDB.GetAllSupContacts(); // get all supplierContacts from db
            displayList(supConList);
        }

        // helper to initialize list
        private void displayList(List<SupplierContact> supCons) {
            lvSupplierContacts.Items.Clear();
            foreach (var sc in supCons) {
                string[] arr = new string[4];
                Supplier sup = new Supplier();
                var sname = (from s in supList
                            where s.SupplierID == sc.SupplierId
                            select s.SupName).FirstOrDefault();
                arr[0] = Convert.ToString(sname);
                arr[1] = sc.SupplierContactId.ToString();
                arr[2] = sc.SupConFirstName.ToString();
                arr[3] = sc.SupConLastName.ToString();
                lvSupplierContacts.Items.Add(new ListViewItem(arr));
            }
        }


        // filters list on every character entry
        private void txtFilter_TextChanged(object sender, EventArgs e) {
            if (txtFilter.Text != "") {
                if (cmboFilterBy.Text == "First Name") { // filter list by first name
                    displayList(filterList('F'));
                } else if (cmboFilterBy.Text == "Last Name") { // filter list by last Name
                    displayList(filterList('L')); 
                } else if (cmboFilterBy.Text == "Supplier") {
                    displayList(filterList('S'));
                }
            } else { // display full list
                displayList(supConList);
            }
        }


        // helper for above event - filters based on three options
        private List<SupplierContact> filterList(char field = 'F') {
            List<SupplierContact> filteredList = null;
            List<SupplierContact> filteredListJoin = new List<SupplierContact>();
            switch (field) {
                case 'F': // filter by first name
                    filteredList = new List<SupplierContact>();
                    filteredList = (from s in supConList
                                    where s.SupConFirstName.ToUpper().StartsWith(txtFilter.Text.ToUpper())
                                    select s).ToList();
                    break;
                case 'L': // filter by last name
                    filteredList = new List<SupplierContact>();
                    filteredList = (from s in supConList
                                    where s.SupConLastName.ToUpper().StartsWith(txtFilter.Text.ToUpper())
                                    select s).ToList();
                    break;
                case 'S':  // filter by supplier name
                    filteredList = new List<SupplierContact>();
                    filteredList = (from s1 in supConList
                                    from s2 in supList
                                    where s1.SupplierId == s2.SupplierID && 
                                          s2.SupName.ToUpper().StartsWith(txtFilter.Text.ToUpper())
                                    select s1).ToList();
                    break;
            }         
            return filteredList;
        }



        private void frmOpenSupplier_Activated(object sender, EventArgs e) {
            displayList(supConList);
        }


        // close the form, no data sent
        private void btnCancel_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
