﻿
/*
 * Author: Don van Hout
 * Date: July, 2017
 * Description:  Splash form, displays progress bar for 
 *               loading and company logo image
 *            
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TravelExperts.Properties;

namespace TravelExperts {
    public partial class frmSplash : Form {
        public frmSplash() {
            InitializeComponent();

            PictureBox spashPictureBox = new PictureBox();
            spashPictureBox.Image = Resources.te_SplashFormImage1;
            spashPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            spashPictureBox.Dock = DockStyle.Fill;
            this.Controls.Add(spashPictureBox);
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void timer1_Tick(object sender, EventArgs e) {
            progressBar1.Increment(1);
            if (progressBar1.Value == 100) {
                timer1.Stop();
            }
        }
    }
}
